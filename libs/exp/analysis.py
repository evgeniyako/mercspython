"""
Analysis - Module that groups general functions used in analyzing finished experiments.

This mostly happens in ipynbs, but some function become standard ones and can be included here.
In this way, the size of ipynbs remains in control, and they contain just the real work on
a specific problem. In this way, that research is easily parseable by looking at said notebook,
without having to scroll through all the functions that are shared in the notebook anyhow.
"""

import numpy as np
import re
from .tools.shared import *


"""Check for failed experiments"""

def check_failures():
    """
    Identify the experiments (by means of ID) that need to be redone.

    This function checks which experiments have a logfile with 'failure' in the filename, or no logfiles at all.
    """

    info = generate_global_names()  # Get the naming conventions
    all_ids = os.listdir(info['path']['out'])
    all_ids.sort()

    fail_ids = []
    for current_id in all_ids:
        id_folder = os.path.join(info['path']['out'], current_id)
        datasets = os.listdir(id_folder)
        for ds in datasets:
            log_folder = os.path.join(id_folder, ds, "logs")
            logs = os.listdir(log_folder)
            fail_logs = [log for log in logs if 'fail' in log]
            if len(fail_logs) > 0:
                fail_ids.append(int(current_id))
            elif len(logs) == 0:
                fail_ids.append(int(current_id))
            else:
                pass

    return fail_ids

def rewrite_inputs_csv(old_file, fail_ids, new_file='update.csv'):
    """
    Rewrite a given .csv file based on the failed IDs.

    Based on IDs that have to be kept, it rewrites the inputs.csv file. This to quickly have
    a new file which can be used to rerun failed experiments.

    However, it assumes all given IDs to present in the inputfile.
    TODO: Fix this.
    """

    old_inputs = pd.read_csv(old_file)  # Read the old
    new_inputs = pd.DataFrame()

    for current_id in fail_ids:
        new_inputs = new_inputs.append(old_inputs[old_inputs.ID == current_id])

    new_inputs.to_csv(new_file, index=False)  # Make a new experimental-params file
    return


"""Actual analysis of experimental results"""

## Global functions

def evaluation_classification(ID, data_loc=None):
    """
    Evaluate a given experiment (classification task).

    For every query, and for every target of that query: compute evaluation metric(s)
    """

    # 1] Collect settings and filenames and useful folders
    info, manifest, system, dataset = get_info(ID, data_loc)

    # 2] Collect the inputdata and is_nominal array of the input.
    truth_folder = info['xval']
    truth, is_nominal, all_classlabels = get_truth(truth_folder, system)

    # 3] Collect the prediction scores made by the ML algorithm.
    preds_folder = info['output']
    desc_sets, targ_sets, miss_sets = get_queries(manifest)

    macro_F1_scores = []
    micro_F1_scores = []
    accuracies = []
    for query_id in range(0, len(desc_sets)):  # Add the evaluation of query #i to the scores array.
        macro_F1_score, accuracy, micro_F1_score = evaluate_single_query_classification(truth, is_nominal, all_classlabels, preds_folder, targ_sets, query_id, system)
        macro_F1_scores.append(macro_F1_score)
        micro_F1_scores.append(micro_F1_score)
        accuracies.append(accuracy)

    return macro_F1_scores, accuracies, micro_F1_scores, is_nominal

def evaluation_regression(ID, data_loc=None):
    """
    Evaluate a given experiment (classification task).

    For every query, and for every target of that query: compute evaluation metric(s)
    """
    # 1] Collect settings and filenames and useful folders
    info, manifest, system, dataset = get_info(ID, data_loc)

    # 2] Collect the inputdata and is_nominal array of the input.
    truth_folder = info['xval']
    truth, is_numerical, all_values = get_truth_regression(truth_folder, system)

    # 3] Collect the predictions made by the ML algorithm.
    preds_folder = info['output']
    desc_sets, targ_sets, miss_sets = get_queries(manifest)

    mean_squared_errors = []
    accuracies = []
    norm_rmses = []

    for query_id in range(0, len(desc_sets)):  # Add the evaluation of query #i to the scores array.
        acc, mse, nrmse = evaluate_single_query_regression(truth,
                                                           is_numerical,
                                                           all_values,
                                                           preds_folder,
                                                           targ_sets,
                                                           query_id,
                                                           system)
        accuracies.append(acc)
        mean_squared_errors.append(mse)
        norm_rmses.append(nrmse)



    return accuracies, mean_squared_errors, norm_rmses, is_numerical

def evaluate_single_query_classification(truth, is_nominal, all_classlabels, preds_folder, targ_sets, query_ID, system):
    """
    Evaluate the performance of a single query.

    For nominal attributes: F1 Score/Accuracy.

    :param query_ID -    int, refers to the query number which result we wish to fetch.

    :return scores -     An array of [target_nb, F1 score]
    :return accuracies - An array of [target_nb, accuracy]
    """

    # TODO: Fix this also for non-nominal vars
    # 1] Collect the prediction DataFrame of the relevant query
    pred_df = get_prediction(preds_folder, query_ID, system)

    # 2] Filter the relevant columns of truth and prediction
    targetset = targ_sets[query_ID]

    true_matrix = filter_truth(truth, targetset)
    is_nom_matrix = filter_is_nom(is_nominal, targetset)

    pred_matrix = filter_pred(pred_df, targetset, system)
    #print(true_matrix.shape)
    #print(pred_matrix.shape)

    assert true_matrix.shape == pred_matrix.shape

    # 3] For every target in the targetset of the query, calculate the performance (e.g. F1 etc)
    macro_F1_scores = [[is_nom_matrix[0, i], macro_F1(true_matrix[:, i], pred_matrix[:, i], all_classlabels[i])]  # [target_nb,F1]
              if is_nom_matrix[1, i]
              else None  # The other thing to do
              for i in range(0, len(targetset))]  # an array of [target_nb,F1/other measure]

    micro_F1_scores = [[is_nom_matrix[0, i], micro_F1(true_matrix[:, i], pred_matrix[:, i], all_classlabels[i])]  # [target_nb,F1]
              if is_nom_matrix[1, i]
              else None  # The other thing to do
              for i in range(0, len(targetset))]  # an array of [target_nb,F1/other measure]

    accuracies = [[is_nom_matrix[0, i], accuracy(true_matrix[:, i], pred_matrix[:, i])]  # [target_nb,acc.]
                  if is_nom_matrix[1, i]
                  else None  # The other thing to do
                  for i in range(0, len(targetset))]  # an array of [target_nb,accuracy]

    return macro_F1_scores, accuracies, micro_F1_scores


def evaluate_single_query_regression(truth, is_numerical, all_values, preds_folder, targ_sets, query_ID, system):
    """
    Evaluate the performance of a single query (regression task).

    For nominal attributes: F1 Score/Accuracy.

    :param query_ID -    int, refers to the query number which result we wish to fetch.

    :return scores -     An array of [target_nb, F1 score]
    :return accuracies - An array of [target_nb, accuracy]
    """

    # 1] Collect the prediction DataFrame of the relevant query
    pred_df = get_prediction(preds_folder, query_ID, system)

    # 2] Filter the relevant columns of truth and prediction
    targetset = targ_sets[query_ID]
    true_matrix = filter_truth(truth, targetset)
    is_num_matrix = filter_is_num(is_numerical, targetset)
    pred_matrix = filter_pred(pred_df, targetset, system)

    assert true_matrix.shape == pred_matrix.shape

    # 3] For every target in the targetset of the query, calculate the performance

    accuracies = [[is_num_matrix[0, i], relative_error(true_matrix[:, i], pred_matrix[:, i])]  # [target_nb,acc]
              if is_num_matrix[1, i]
              else None  # The other thing to do
              for i in range(0, len(targetset))]  # an array of [target_nb,F1/other measure]

    mse = [[is_num_matrix[0, i], get_mse(true_matrix[:, i], pred_matrix[:, i])]  # [target_nb,sum(res^2).]
                  if is_num_matrix[1, i]
                  else None  # The other thing to do
                  for i in range(0, len(targetset))]  # an array of [target_nb,accuracy]
    nrmse = [[is_num_matrix[0,i], get_nrmse(true_matrix[:,i], pred_matrix[:, i])]
             if is_num_matrix[1,i]
             else None
             for i in range (0, len(targetset))]

    return accuracies, mse, nrmse





def write_results_classification(file, macro_F1s, accuracies, micro_F1s, nominal_vars, induction, inference, nb_queries):
    """
    Writing global results file.

    This allows us to easy access the results without having to go back to the data all the time
    """

    targetsets = [np.array(score)[:, 0].tolist() for score in macro_F1s]  # One array that contains arrays of targetsets of all the queries.
    macro_F1_per_targetset = [np.array(score)[:, 1].tolist() for score in macro_F1s]
    micro_F1_per_targetset = [np.array(score)[:, 1].tolist() for score in micro_F1s]
    acc_per_targetset = [np.array(acc)[:, 1].tolist() for acc in accuracies]
    nominal_vars = nominal_vars.values.tolist()
    # NB.: tolist is necessary because of JSON conversion

    exp_results = {
        'targetsets': targetsets,
        'macro_F1_per_targetset': macro_F1_per_targetset,
        'micro_F1_per_targetset': micro_F1_per_targetset,
        'accuracies_per_targetset': acc_per_targetset,
        'induction_time': induction,
        'inference_time': inference,
        'nb_queries': nb_queries,
        'nominal_vars': nominal_vars
    }

    # Writing it to a settings file
    with open(file, 'w') as f:
        json.dump(exp_results, f)
    return

def write_results_regression(file, accuracies, mses, nrmses, numerical_vars, induction, inference, nb_queries):
    """
    Writing global results file.

    This allows us to easy access the results without having to go back to the data all the time
    """
    # Array that contains arrays of targetsets of all the queries.
    targetsets = [np.array(target)[:, 0].tolist() for target in accuracies]

    # Array that contains accuracies per targetset
    relative_error_per_targetset = [np.array(acc)[:, 1].tolist() for acc in accuracies]

    # Array that contains MSEs per targetset
    mse_per_targetset = [np.array(mse)[:, 1].tolist() for mse in mses]

    # Array that contains MSEs per targetset
    nrmse_per_targetset = [np.array(nrmse)[:, 1].tolist() for nrmse in nrmses]

    numerical_vars = numerical_vars.values.tolist()
    # NB.: tolist is necessary because of JSON conversion

    exp_results = {
        'targetsets': targetsets,
        'relative_error_target': relative_error_per_targetset,
        'mse_target': mse_per_targetset,
        'nrmse_target': nrmse_per_targetset,
        'induction_time': induction,
        'inference_time': inference,
        'nb_queries': nb_queries,
        'numerical_vars': numerical_vars
    }

    # Writing it to a settings file
    with open(file, 'w') as f:
        json.dump(exp_results, f)
    return


def analyze_and_save_all(IDs=None, inputsfile=None, data_loc=None):
    """
    Analyze all the experiments specified and save the results.

    :param IDs -        [int], an array of the IDs you want to be analyzed
    :param inputsfile - string, analyzes all the IDs specified in that file.
    """

    if (IDs == None and inputsfile == None) or ((IDs != None and inputsfile != None)): print(
        'Please, you have to provide strictly one of \{inputsfile,IDs\}.')

    if (inputsfile != None):
        res = get_settings(inputsfile)  # IDs can be fetched from the inputsfile.
        IDs = res.index.values

    for ID in IDs:
        print(str(ID))
        analyze_and_save(ID, data_loc)
        print('Finished analysis of ID: ' + str(ID))

    return

def analyze_and_save(ID, data_loc=None, task = 'regression'):
    """
    Analyze a single experiment and save the results.

    This function basically collects some fs info, evaluates the results and saves them.
    """

    # 1] Preliminaries (for location of results file)
    info, _, _, _ = get_info(ID, data_loc)
    if (task == 'classification'):
        # 2] Collect relevant information
        macro_F1_scores, accuracies, micro_F1_scores, nominal_vars = evaluation_classification(ID, data_loc)
        induction, inference, nb_queries = read_log_files(ID, data_loc)

        file = os.path.join(info['results'], 'results.json')
        write_results_classification(file, macro_F1_scores, accuracies, micro_F1_scores, nominal_vars, induction, inference, nb_queries)
    else:
        # 2] Collect relevant information
        accuracies, mses, nrmses, is_numerical = evaluation_regression(ID, data_loc)
        induction, inference, nb_queries = read_log_files(ID, data_loc)

        file = os.path.join(info['results'], 'results.json')
        write_results_regression(file, accuracies, mses, nrmses, is_numerical, induction, inference, nb_queries)
    return


## Data collection (results) and filtering

def get_truth(folder, system='mercs'):
    """
    Collect the original, correct values to compare the prediction with.

    Additionally, we fetch information about nominal attributes and about the classlabels of such nominal
    attributes.

    :param folder: Folder that contains the testdata.
    :param system: BayesFusion (smile) or Mercs
    :return res: A DataFrame with the original data
    :return is_nominal: A DataFrame that contains information about wich target is nominal or not.
    :return classlabels: Array of arrays. The ith entry contains an array [class_label_0, ...] about target i.
    """

    # List all the files in the folder that contain the system and 'Test'
    test_data = [os.path.join(folder, name) for name in os.listdir(folder) if system+'_' in name if 'Test' in name]
    test_data.sort()

    res = pd.DataFrame()  # Init DataFrame

    if system == 'mercs':
        for file in test_data:  # Loading in the actual datafiles
            _, data = import_arff(file)
            res = res.append(data)
    elif ((system == 'smile') | (system == 'bayesfusion') | (system == 'mercspython')):
        for file in test_data:
            data = pd.read_csv(file)
            res = res.append(data)

    # We need to get some information from the .arff file anyways.
    arff_data = [os.path.join(folder, name) for name in os.listdir(folder) if 'mercs_' in name if 'Test' in name]
    header, _ = import_arff(arff_data[0])
    classlabels = get_classlabels(arff_data[0])
    res.columns = [n for n, v in header]  # Filling in variable names
    # Extra info: a df that contains whether or not the variable is nominal
    is_nominal = np.array([[i, type(v) == list] for i, (n, v) in enumerate(header)]).T
    is_nominal = pd.DataFrame(is_nominal, columns=res.columns, index=['target_id', 'is_nominal'])

    return res, is_nominal, classlabels

# truth, is_numerical, all_values = get_truth_regression(truth_folder, system)

def get_truth_regression(folder, system='mercs'):
    """
    Collect the original, correct values to compare the prediction with.

    Additionally, we fetch information about nominal attributes and about the classlabels of such nominal
    attributes.

    :param folder: Folder that contains the testdata.
    :param system: BayesFusion (smile) or Mercs
    :return res: A DataFrame with the original data
    :return is_nominal: A DataFrame that contains information about wich target is nominal or not.
    :return classlabels: Array of arrays. The ith entry contains an array [class_label_0, ...] about target i.
    """

    # List all the files in the folder that contain the system and 'Test'
    test_data = [os.path.join(folder, name) for name in os.listdir(folder) if system+'_' in name if 'Test' in name]
    test_data.sort()

    res = pd.DataFrame()  # Init DataFrame

    if system == 'mercs':
        for file in test_data:  # Loading in the actual datafiles
            _, data = import_arff(file)
            res = res.append(data)
    elif ((system == 'smile') | (system == 'bayesfusion') | (system == 'mercspython')):
        for file in test_data:
            data = pd.read_csv(file)
            res = res.append(data)

    # We need to get some information from the .arff file anyways.
    arff_data = [os.path.join(folder, name) for name in os.listdir(folder) if 'mercs_' in name if 'Test' in name]
    header, _ = import_arff(arff_data[0])
    all_values = get_classlabels(arff_data[0])
    res.columns = [n for n, v in header]  # Filling in variable names

    # Extra info: a df that contains whether or not the variable is nominal
    is_numerical = np.array([[i, 1] for i, (n, v) in enumerate(header)]).T
    is_numerical = pd.DataFrame(is_numerical, columns=res.columns, index=['target_id', 'is_numerical'])

    return res, is_numerical, all_values

def get_prediction(folder, query_nb, system='mercs'):
    """
    Collect the predictions for a certain query.

    The system + query ID tell this function which files to combine in a df.
    """

    # List all the files in the folder that contain the system and the right query number
    targetcode = get_code(query_nb, 'target')
    pred_data = [os.path.join(folder, name) for name in os.listdir(folder) if system+'_' in name if targetcode in name]
    pred_data.sort()

    # Load the files and collect their outputs in a DataFrame
    res = pd.DataFrame()

    for file in pred_data:
        data = pd.read_csv(file)
        res = res.append(data)

    return res

def filter_truth(df_truth, targetset):
    """
    Filter the truth DataFrame. Based on targetset of a query.

    Keep only the real values of the attributes that were predicted in the given query.
    This is system-independent. (The ground truth is always the same file)
    """

    df = df_truth.iloc[:, targetset]
    true_matrix = df.values

    return true_matrix

def filter_is_nom(df_is_nom, targetset):
    """
    Filter the is_nominal DataFrame, based on targetset of a query.

    This is system-independent. (The ground truth is always the same file)
    """

    df = df_is_nom.iloc[:, targetset]
    is_nom_matrix = df.values

    return is_nom_matrix

def filter_is_num(df_is_num, targetset):
    """
    Filter the is_nominal DataFrame, based on targetset of a query.

    This is system-independent. (The ground truth is always the same file)
    """

    df = df_is_num.iloc[:, targetset]
    is_num_matrix = df.values

    return is_num_matrix

def filter_pred(df_pred, targetset, system):
    """
    Filter the predictions DataFrame, based on targetset of a query.

    Keep only the attributes that were predicted in this query
    """

    if system == 'mercs':
        df = df_pred
    elif ((system == 'smile') | (system =='bayesfusion')):
        original_cols = list(df_pred)  # List of all my column names
        relevant_cols = [col for col in original_cols if '_predicted' in col]  # Extracting exactly the ones that hold predictions
        df = df_pred[relevant_cols]
    elif system == 'mercspython':
        df = df_pred
    else:
        print('Did not recognize system!')

    pred_matrix = df.values

    return pred_matrix


## Meta-data collection (logs) and filtering

def read_log_files(ID, data_loc=None):
    """
    Read the log files: extract induction, inference and number of queries.

    This function actually reads the logs and extracts the information that we want out of it.
    """

    # 1] Obtain the filenames of the logs
    info, manifest, system, dataset = get_info(ID, data_loc)

    logs_folder = info['logs']
    filenames = [os.path.join(logs_folder, name) for name in os.listdir(logs_folder) if system+'_' in name if
                 'success.log' in name]

    # 2] Extract information from the logs
    log_info = [read_one_log_file(file) for file in filenames]
    ind, inf, nbq = [ind for ind, inf, nbq in log_info], [inf for ind, inf, nbq in log_info], [nbq for ind, inf, nbq in
                                                                                               log_info]

    # Compute averages over all the folds.
    induction, inference, nb_queries = np.mean(ind), np.mean(inf), np.mean(nbq)

    return induction, inference, nb_queries

def read_one_log_file(file):
    """
    Extract the useful data from one log file.

    This supposes fixed format outputs.
    """

    with open(file, 'r') as f:
        for num, line in enumerate(f):
            if "induction time" in line:
                induction_time = [int(s) for s in line.split() if s.isdigit()]

            if "inference time" in line:
                inference_time = [int(s) for s in line.split() if s.isdigit()]

            if "This was for" in line:
                n_queries = [int(s) for s in line.split() if s.isdigit()]

    return induction_time[0], inference_time[0], n_queries[0]


def get_nb_queries(folder):
    """
    Automatically collect the number of targetsets used in this experiment.

    Based on the filenames of the predictions.
    """
    regex = r"(T\d+)"
    filenames = [re.search(regex, name).group(1) for name in os.listdir(folder)]
    n = len(np.unique(filenames))

    return n

def convert_to_matrices(truth, prediction):
    """
    Convert two DataFrames.

    INPUT: truth, prediction: Two DataFrames containing all the input data and the predicted values.
    OUTPUT:true, pred:        Two NP arrays, containing only the true and predicted values.
    """

    # 1] Create a DataFrame that contains the True and Predicted values
    # Filter the relevant columns from the true data
    cols_pred = prediction.columns.tolist()
    filtered_truth = truth[cols_pred].copy()

    # Renaming the column headers to clearly indicate which is which
    filtered_truth.columns = ['truth_' + name for name in filtered_truth.columns.tolist()]
    prediction.columns = ['pred_' + name for name in prediction.columns.tolist()]

    # Merge into one dataframe
    res = pd.concat([filtered_truth, prediction], axis=1)
    matrix = res.values

    # 2] Do the calculations you want to do
    border = len(cols_pred)
    true, pred = matrix[:, :border], matrix[:, border:]

    return true, pred

def get_info(ID, data_loc=None):
    """
    Get dictionary with the folder structure
    """

    # 1] Get our settings for a particular ID
    # TODO: Seperate manifest from queries, to make sure this is not more heavy than necessary.
    mf = read_manifest(ID)

    system, dataset = mf['system'], mf['dataset']  # Very basic extractions

    # 2] Generate fs information
    info = generate_all_names(ID, dataset, data_location=data_loc, system=system)

    return info, mf, system, dataset

def get_settings(file, ID=None):
    """
    Collect the settings/parameters of a given experiment.

    Read the csv file that gives all the major parameters for an experiment. With a particular ID, or just all of them.
    """

    all_settings = pd.read_csv(file, index_col=0)

    if ID == None:
        res = all_settings
    else:
        res = all_settings.loc[ID]

    return res

def get_metadata_dataset(filename):
    """
    Get the attribute and tuple count of a certain arff file.
    """

    nb_tuples, _ = count_lines(filename)
    atts = count_values(filename)
    return nb_tuples, atts

def get_metadata_experiment(folder, system):
    """
    Get some metadata about the data used in this experiment
    """

    res, nominal_vars, _ = get_truth(folder, system)  # Two DataFrames with all the information I need.

    nb_tuples = len(res.index)
    nb_atts = len(res.columns)

    true_matrix = res.values
    values = [np.unique(true_matrix[:, i]) for i in range(0, nb_atts)]
    nb_values = [len(item) for item in values]

    return nb_tuples, nb_atts, values, nb_values


## Performance metrics

def micro_F1(truth, prediction, classlabels):
    """
    Compute the macro F1 score for ONE variable. This means the inputs are one-dimensional

    :param truth: numpy array of the actual values
    :param prediction: numpy array of the predicted values
    :param classlabels: array of possible classlabels for this particular attribute
    :return: macro F1 score for one variable, real number
    """

    if len(classlabels) > 2:
        micro_prec, micro_rec = micro_precision_recall(truth, prediction, classlabels)
    else:
        micro_prec, micro_rec = precision_recall(truth, prediction, 1) # In this case, regular P/R is good.

    microF1 = calc_F1(micro_prec, micro_rec)

    return microF1

def calc_F1(precision, recall):
    f1 = 2 * (precision * recall) / (precision + recall) if (precision + recall) != 0 else 0
    return f1


def macro_F1(truth, prediction, classlabels):
    """
    Compute the macro F1 score for ONE variable. This means the inputs are one-dimensional

    :param truth: numpy array of the actual values
    :param prediction: numpy array of the predicted values
    :param classlabels: array of possible classlabels for this particular attribute
    :return: macro F1 score for one variable, real number
    """

    if len(classlabels) > 2:
        macro_prec, macro_rec = macro_precision_recall(truth, prediction, classlabels)
    else:
        macro_prec, macro_rec = precision_recall(truth, prediction, 1) # In this case, regular P/R is good.

    macroF1 = calc_F1(macro_prec, macro_rec)
    return macroF1

def macro_precision_recall(truth, prediction, classlabels=None):
    """
    Compute the macro precision and recall for this attribute.

    :param truth: numpy array of the actual values
    :param prediction: numpy array of the predicted values
    :param classlabels: array of possible classlabels for this particular attribute
    :return: precision and recall
    """

    if classlabels == None:
        classlabels = np.unique(truth)  # Collect all possible classes.
    else:
        pass

    nb_classes = len(classlabels)

    prec_rec = np.array(list(precision_recall(truth, prediction, class_id) for class_id in classlabels))
    precisions = prec_rec[:, 0]
    recalls = prec_rec[:, 1]

    macro_precision, macro_recall = np.sum(precisions) / nb_classes, np.sum(recalls) / nb_classes

    return macro_precision, macro_recall


def micro_precision_recall(truth, prediction, classlabels):
    if classlabels == None:
        classlabels = np.unique(truth)  # Collect all possible classes.
    else:
        pass
    nb_classes = len(classlabels)
    assert nb_classes>2

    TP_total, FP_total, FN_total = 0, 0, 0

    for class_id in classlabels:
        TP, FP, FN, TN = calc_tp_fp_fn_tn(truth, prediction, class_id)
        TP_total += TP
        FP_total += FP
        FN_total += FN
    micro_precis = TP_total / (TP_total + FP_total) if TP_total + FP_total != 0 else 0
    micro_recall = TP_total / (TP_total + FN_total) if TP_total + FN_total != 0 else 0

    return micro_precis, micro_recall


def precision_recall(truth, prediction, class_id):
    """
    Compute precision recall for one class_id.


    This screws up the macro score, which is an average, but we want to spot this behaviour nonetheless. Currently we just put precision to zero.
    """

    TP, FP, FN, TN = calc_tp_fp_fn_tn(truth, prediction, class_id)

    precision = TP / (TP + FP) if TP + FP != 0 else 1
    recall = TP / (TP + FN) if TP + FN != 0 else 1

    return precision, recall


def calc_tp_fp_fn_tn(true, pred, class_id):
    """
    Calculate the confusion matrix for a binary problem.

    :param true:
    :param pred:
    :param class_id:
    :return:
    """

    nb_tuples = len(true)
    assert nb_tuples == len(pred)

    # Make boolean arrays with regard to the class
    true = [1 if true[i] == class_id else 0 for i in range(nb_tuples)]
    pred = [1 if pred[i] == class_id else 0 for i in range(nb_tuples)]

    TP = np.sum(np.logical_and(true, pred))
    FP = np.sum(np.logical_and(np.logical_not(true), pred))
    FN = np.sum(np.logical_and(true, np.logical_not(pred)))
    TN = np.sum(np.logical_and(np.logical_not(true), np.logical_not(pred)))

    return TP, FP, FN, TN


def accuracy(truth, pred):
    """
    Function that calculates the accuracy of a given prediction.

    :param: truth np.array (1D) that contains the real values
    :param: prediction np.array that contains the predicted values
    """

    nb_tuples = len(truth)
    assert nb_tuples == len(pred)

    correct = [1 if truth[i] == pred[i] else 0 for i in range(nb_tuples)]

    accuracy = np.sum(correct) / nb_tuples
    return accuracy

def relative_error(truth, pred):
    """
    Function that calculates the accuracy of a given prediction (regression task).

    :param: truth np.array (1D) that contains the real values
    :param: prediction np.array that contains the predicted values
    """
    nb_tuples = len(truth)
    assert nb_tuples == len(pred)

    relative_error = 0
    for i in range(nb_tuples):
        relative_error = relative_error + np.abs(truth[i] - pred[i])/(truth[i]+1)

    relative_error =  relative_error/nb_tuples
    return relative_error


def get_mse(truth, pred):
    """
    Function that calculates the average of residuals^2 of a given prediction (regression task).
    (MSE error)
    :param: truth np.array (1D) that contains the real values
    :param: prediction np.array that contains the predicted values
    """
    nb_tuples = len(truth)
    assert nb_tuples == len(pred)

    sum_residuals = 0
    for i in range(nb_tuples):
        sum_residuals = sum_residuals + (truth[i] - pred[i])**2

    mse = sum_residuals/nb_tuples

    return mse

def get_nrmse(truth, pred):
    """
    Function that calculates normalized root mean squared error of a given prediction (regression task).
    (NRMSE error)
    :param: truth np.array (1D) that contains the real values
    :param: prediction np.array that contains the predicted values
    """

    mse = get_mse(truth,pred)
    nrmse = np.sqrt(mse)/np.mean(truth)

    return nrmse

def get_R2(truth, pred):
    """
    Function that calculates the sum of residuals^2 of a given prediction (regression task).

    :param: truth np.array (1D) that contains the real values
    :param: residuals_squared float that contains sum od squared residuals
    """
    nb_tuples = len(truth)

    SSres = 0
    for i in range(nb_tuples):
        SSres = SSres + (truth[i]-pred[i])**2

    SStot = np.var(truth)*nb_tuples

    R2 = 1 - SSres/SStot

    return R2

"""Collecting results of analysis, create DataFrames that summarize the results"""

## Performance metrics in DataFrames

def get_exp_df_all(IDs=None, inputsfile=None, data_loc=None):
    """
    Build a DataFrame that contains global statistics of the provided experiments.

    This DataFrame contains the global results of an experiment. I.e. one line of information per experiment.
    """

    if (IDs == None and inputsfile == None) or ((IDs != None and inputsfile != None)): print(
        'Please, you have to provide strictly one of these two.')

    if (inputsfile != None):
        res = get_settings(inputsfile)  # IDs can be fetched from the inputsfile.
        IDs = res.index.values

    df = pd.DataFrame()
    for ID in IDs: df = df.append(get_exp_df(ID, data_loc))

    return df


def get_exp_df(ID, data_loc=None, task='regression'):
    """
    Build a DataFrame that summarizes one experiment.

    So, this creates one line to be used in the get_exp_df_all method.
    """

    # 1] Read the results .json file
    info, manifest, system, dataset = get_info(ID, data_loc)
    settings = flatten_manifest_to_settings_dict(manifest)

    xval_folder = info['xval']
    res_file = os.path.join(info['results'], 'results.json')
    with open(res_file) as f:
        res = json.load(f)

    # 2] Build the DF
    d = {}  # Temporary dictionary that we use to fill the df

    ## Settings stuff (everything extracted from the manifest that is not a list goes in here!)
    d = settings

    ## Global stuff
    d['System'], d['ID'], d['Dataset'] = system, ID, dataset

    ## Dataset-specific
    d['Nb Tuples'], d['Nb Atts'], values, nb_values = get_metadata_experiment(xval_folder, system)

    ## Performance related
    d['Ind t'], d['Inf t'], d['Tot t'] = res['induction_time'], res['inference_time'], res['induction_time'] + res[
        'inference_time']
    if (task == 'classification'):
        d['# Val/Att'] = np.sum(nb_values) / d['Nb Atts']
        macro_F1_scores = res['macro_F1_per_targetset']
        micro_F1_scores = res['micro_F1_per_targetset']
        accuracies = res['accuracies_per_targetset']
        d['Nb Queries'] = len(res['targetsets'])
        d['F1/Query'] = np.sum(macro_F1_scores) / d['Nb Queries']
        d['micro_F1/Query'] = np.sum(micro_F1_scores) / d['Nb Queries']
        d['Acc/Query'] = np.sum(accuracies) / d['Nb Queries']
    else:
        errs = res['relative_error_target']
        mse = res['mse_target']
        nrmse = res['nrmse_target']
        d['Nb Queries'] = len(res['targetsets'])
        d['Relative error / query'] = np.sum(errs)/d['Nb Queries']
        d['MSE / query'] =  np.sum(mse)/d['Nb Queries']
        d['RMSE / query'] = np.sum(np.sqrt(mse)) / d['Nb Queries']
        d['NRMSE / query'] = np.sum(nrmse) / d['Nb Queries']
        d['NRMSEs'] = []
        #NRMSEs = [y for x in nrmse.tolist() for y in x]
        NRMSEs = [y for x in nrmse for y in x]
        d['NRMSEs'].append(NRMSEs)
    # Re-arranging the columns.
    # cols=['ID','System','Dataset','Nb Tuples','Nb Atts','# Val/Att','basetype','ensemble','ens_its','selection','iterations','prediction','Nb Queries','Ind t','Inf t','Tot t','F1/Query','Acc/Query','query_param']
    df = pd.DataFrame(d, index=[0])
    df.set_index('ID', inplace=True)

    return df


def get_exp_df_single(ID, data_loc=None, task = 'regression'):
    """
    Build a DataFrame that collects all the information about a single experiment.

    This DataFrame contains fine-grained information. Contains performance measure for every, single query of said experiment.
    """

    # 1] Read the results .json file
    info, manifest, system, dataset = get_info(ID, data_loc)  # Some general function to obtain useful filesystem information etc.
    settings = flatten_manifest_to_settings_dict(manifest)

    mf = read_manifest(ID)
    desc, targ, miss = get_queries(mf)
    missing = [len(x) for x in miss]

    xval_folder = info['xval']
    res_file = os.path.join(info['results'], 'results.json')
    with open(res_file) as f:
        res = json.load(f)

    # 2] Build the DF
    d = {}  # Temporary dictionary that we use to fill the df
    e = {}

    # Settings stuff
    d = settings

    # Global stuff
    d['System'], d['ID'], d['Dataset'] = system, ID, dataset

    # Dataset-specific
    d['Nb Tuples'], d['Nb Atts'], values, nb_values = get_metadata_experiment(xval_folder, system)
    d['# Val/Att'] = np.sum(nb_values) / d['Nb Atts']

    # Performance related
    d['Ind t'], d['Inf t'], d['Tot t'] = res['induction_time'], res['inference_time'], res['induction_time'] + res['inference_time']

    if (task == 'classification'):
        macro_F1s = [e[0] for e in res['macro_F1_per_targetset']]
        micro_F1s = [e[0] for e in res['micro_F1_per_targetset']]
        accuracies = [e[0] for e in res['accuracies_per_targetset']]
        d['Nb Queries'] = len(res['targetsets'])
        d['F1'] = macro_F1s # TODO: This is inconsistent nomenclature
        d['micro_F1'] = micro_F1s
        d['Acc'] = accuracies
        d['ts_ID'] = range(len(res['targetsets']))
        d['missing'] = missing
        d['target'] = targ
    else:
        accuracies = [e[0] for e in res['relative_error']]
        R2s = [e[0] for e in res['R2_target']]
        residuals = [e[0] for e in res['sum_of_res_target']]
        d['Nb Queries'] = len(res['targetsets'])
        d['R2'] = R2s  # TODO: This is inconsistent nomenclature
        d['Res'] = residuals
        d['Acc'] = accuracies
        d['ts_ID'] = range(len(res['targetsets']))
        d['missing'] = missing
        d['target'] = targ

    # Re-arranging the columns.
    # cols=['ts_ID','ID','System','Dataset','Nb Tuples','Nb Atts','# Val/Att','basetype','ensemble','ens_its','selection','iterations','prediction','Ind t','Inf t','Tot t','F1','Acc','query_param']
    df = pd.DataFrame(d)
    df.set_index('ts_ID', inplace=True)

    return df


## MercsModels

def get_mercs_model(ID, data_loc=None):
    """
    Read the .json that contains the MercsModel.

    :return desc - [[desc],[desc]], array of arrays. Each [desc] contains the indices of descriptive attributes of one component model.
    :return targ - [[targ],[targ]], array of arrays. Each [targ] contains the indices of target attributes of one component model.
    """

    # 1] Read the results .json file
    info, _, system, _ = get_info(ID, data_loc)  # Some general function to obtain useful filesystem information etc.
    assert system == 'mercs'  # After all, we are interested in the Mercs model...

    # 2] Build the DF
    d = {}
    models_folder = info['models']

    filenames = [os.path.join(models_folder, name) for name in os.listdir(models_folder) if 'mercs_' in name]
    filenames.sort()

    desc = []
    targ = []

    for file in filenames:
        with open(file) as f:
            res = json.load(f)
            desc += res['Desc']
            targ += res['Targ']

    return desc, targ

def flatten_model(desc, targ):
    """
    Flatten the desc, targ arrays returned by get_mercs_model.

    :param desc: desc returned by get_mercs_model
    :param targ: targ returned by get_mercs_model
    :return: flat_desc: [int], flat array that contains all the desc attributes that the mercsmodel contains in its component models.
    :return: flat_targ: [int], flat array that contains all the target attributes that the mercsmodel contains in its component models.
    """

    flat_desc = [val for sublist in desc for val in sublist]
    flat_targ = [val for sublist in targ for val in sublist]
    return flat_desc, flat_targ


## Computing derived quantities in the DataFrames

def calc_mean_time_query(df):
    """
    Add a column with the average time per query
    """

    relevant = df[['Inf t', 'Nb Queries']].values
    mean_time = relevant[:, 0] / relevant[:, 1]

    df2 = df.copy()

    df2['Mean time/Query'] = pd.Series(mean_time, index=df.index)  # Do not forget the index!
    return df2

def alter_timescale(df, reduce_factor):
    """
    Convert the timings from ms
    """

    # For instance of ms to s: reduce_factor =1000
    rescaler = lambda x: x / reduce_factor

    df2 = df.copy()

    df2['Ind t'] = df['Ind t'].map(rescaler)
    df2['Inf t'] = df['Inf t'].map(rescaler)
    df2['Tot t'] = df['Tot t'].map(rescaler)
    return df2

def change_basetype(df):
    res = df.copy()

    res.basetype[res.basetype != 'TDIDT'] = res['ensemble']
    res.basetype[res.System == 'smile'] = res['System']

    return res