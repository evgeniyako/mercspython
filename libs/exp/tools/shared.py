import os

import mmap
import pandas as pd
import shutil
import json


from ..libs import arff

# Create the filesystem

def generate_all_names(ID, dataset, data_location=None, system=None):
    """
    Generate all the naming conventions for a certain experiment ID.

    This consists of three steps.
    """

    # 1] Generate all the global stuff, independent of experiment and dataset.
    fs = generate_global_names(data_location)

    # 2] Generate experiment-specific stuff
    fs = generate_exp_names(fs, ID, system)

    # 3] Generate dataset-specific stuff
    fs = generate_dataset_names(fs, dataset)

    return fs

def generate_global_names(data_location=None):
    """
    Generate everything in the filesystem that is independent of

        a] experiment
        b] dataset

    So this basically boils down to creating the archetypical folder structure that is common to any experiment.
    For small things this seems like crazy overhead, but it does create a nice consistency along the way.

    The main structure is this:
        -bin:   Binaries used by the scripts
        -data:  The data used in the experiments. (Contains also the output data produced in experiments.)
        -libs:  The libraries/packages used by the experiments
        -out:   Everything produced during the experiments
        -src:   Source code, i.e.: notebooks and scripts.
    """

    fs = {}
    fs['path'] = {}  # This will contain all the paths of subfolders

    # Main folder
    src_folder = os.path.dirname(os.getcwd())       # Current file is in a subfolder 'ipynb' or 'core' of the src directory
    main_folder = os.path.dirname(src_folder)       # Main folder of the experiment (= where everything runs)
    _, current_exp = os.path.split(main_folder)     # Extract the name of the current experiment

    fs['path']['main'] = main_folder

    # Bin folder
    fs['path']['bin'] = os.path.join(fs['path']['main'], "bin")

    # Data folder
    if data_location == None:
        # Default is main/data folder.
        data_folder = os.path.join(main_folder, "data")
    else:
        # Else the datafolder has to be explicitly provided.
        data_folder = os.path.join(data_location, current_exp)  # An explicit location is provided.

    fs['path']['data'] = data_folder
    fs['path']['raw'] = os.path.join(fs['path']['data'], "raw")
    fs['path']['clean'] = os.path.join(fs['path']['data'], "clean")
    fs['path']['output'] = os.path.join(fs['path']['data'], "output")
    fs['path']['data_log'] = os.path.join(fs['path']['data'], "data_log")

    fs['path']['preproc'] = os.path.join(fs['path']['data'], "preproc")
    fs['path']['xval'] = os.path.join(fs['path']['preproc'], "crossval")

    # Libs, out and src folder
    fs['path']['libs']= os.path.join(fs['path']['main'], "libs")
    fs['path']['out'] = os.path.join(fs['path']['main'], "out")
    fs['path']['src'] = os.path.join(fs['path']['main'], "src")

    """
    IMPORTANT: WE ALSO ENSURE THE EXISTENCE OF THE OUTPUT FOLDERS
    """
    check_dir([fs['path']['output'], fs['path']['out']])  # The 2 global output folders have to exist.

    return fs

def generate_exp_names(fs, ID, system=None):
    """
    Generate everything in the filesystem that is:

    Dependent on: experiment

    Independent of: dataset
    """

    eID_code = get_code(ID, 'exp')
    fs['ID'] = ID
    fs['ID_code'] = eID_code
    fs['system'] = system

    # System specific names
    if system == 'mercs':
        fs = generate_names_mercs(fs)
    elif ((system == 'smile') | (system == 'bayesfusion')):
        fs = generate_names_smile(fs)
    elif system=='mercspython':
        fs = generate_names_mercspython(fs)
    elif system == None:
        pass
    else:
        print('Did not recognize system/algorithm!')

    # Experiment-specific folders
    fs['exp'] = {}

    # Everything actual data (I/O) related (data->'output')
    fs['exp']['output'] = os.path.join(fs['path']['output'], eID_code)

    # Everything algorithm related (results->'out')
    fs['exp']['out'] = os.path.join(fs['path']['out'], eID_code)

    return fs

def generate_dataset_names(fs, dataset):
    """
    Generate dataset-specific names.

        This is done on a lower level than the experiment ID level. This reflects the actual situation in the filesystem.
        Beware that this function writes out on the first level of the dict. This because this is the last one to get called
        within an experiment ID, and wihtin a dataset.

        So, by writing it on the first level, makes things easier. This provides easy access to the experiment at hand.
    """

    # 1] Everything actual data (I/O) related
    fs['raw'] = os.path.join(fs['path']['raw'], dataset)
    fs['clean'] = os.path.join(fs['path']['clean'], dataset)
    fs['preproc'] = os.path.join(fs['path']['preproc'])
    fs['xval'] = os.path.join(fs['path']['xval'], dataset)

    fs['output'] = os.path.join(fs['exp']['output'], dataset)  # All the output data

    # 2] Everything algorithm related
    fs['out'] = os.path.join(fs['exp']['out'], dataset)  # Parent folder with all the outputs (i.e. logs, models, settings)

    fs['logs'] = os.path.join(fs['out'], 'logs')
    fs['models'] = os.path.join(fs['out'], 'models')
    fs['results'] = os.path.join(fs['out'], 'results')
    fs['settings'] = os.path.join(fs['out'], 'settings')

    return fs

#TODO: THIS DOES NOT ALWAYS WORK SO FIX THIS!
def delete_old_exps(fs):
    """
    Helper function: ensuring clean folders.

    Making sure all the output folders are clean to begin with, before conducting a new experiment.
    """

    """
    We check the existence of the experiment ID folder. If it happens to be the first experiment, we make it.
    """
    make_fresh_dir(([fs['exp']['output'], fs['exp']['out']]))

    """
    WE CREATE THESE FOLDERS FRESH.
    THIS MEANS DELETING EXISTING ONES!
        If they already exist, that means same experiment on same dataset. That is a no-go.
    """
    check_dir([fs['output'], fs['out']])
    make_fresh_dir([fs['logs'], fs['models'], fs['results'], fs['settings'], fs['output']])
    return fs

# System-specific functions

def generate_names_mercs(fs):
    """
    Filenames in case of Mercs system.

    Contains all the extensions.
    """

    fs['mercs'] = {}
    binary_name = 'miniMercs'

    fs['mercs']['ext'] = {'in': '.arff', 'out': '.csv', 'settings': '.s', 'exc': '.jar', 'model': '.json'}
    fs['mercs']['name'] = binary_name + fs['mercs']['ext']['exc']

    fs['mercs']['bin'] = os.path.join(fs['path']['bin'], fs['mercs']['name'])
    fs['mercs']['model'] = gen_fname('mercs', 'model', fs['mercs']['ext']['model'])

    return fs

def generate_names_mercspython(fs):
    """
    Filenames in case of MercsPython system.

    Contains all the extensions.
    """

    fs['mercspython'] = {}
    binary_name = 'mercspython'

    fs['mercspython']['ext'] = {'in': '.csv', 'out': '.csv', 'settings': '.s', 'exc': '.py', 'model': '.json'}
    fs['mercspython']['name'] = binary_name + fs['mercspython']['ext']['exc']

    fs['mercspython']['bin'] = os.path.join(fs['path']['bin'], fs['mercspython']['name'])
    fs['mercspython']['model'] = gen_fname('mercspython', 'model', fs['mercspython']['ext']['model'])

    return fs

def generate_names_smile(fs):
    """
    This function generates relevant names when the SMILE system (BayesFusion) is used
    """
    fs['smile'] = {}
    binary_name = 'SmileRunner'

    fs['smile']['ext'] = {'in': '.csv', 'out': '.csv', 'settings': '.json', 'exc': '.jar', 'model': '.xdsl'}
    fs['smile']['name'] = binary_name + fs['smile']['ext']['exc']

    fs['smile']['lib'] = fs['path']['bin']  # Location of the .so library of the smile system
    fs['smile']['bin'] = os.path.join(fs['path']['bin'], fs['smile']['name'])

    fs['smile']['model'] = gen_fname('smile', 'model', fs['smile']['ext']['model'])
    return fs

# Filessytem actions

def check_dir(dirs):
    """
    If dir d does not exist, make it.
    """
    for d in dirs:
        if os.path.exists(d):
            pass
        else:
            os.makedirs(d)
    return

def make_fresh_dir(dirs):
    """
    Ensure that AN EMPTY dir d exists.
    """
    for d in dirs:
        if os.path.exists(d):
            shutil.rmtree(d)  # Delete the directory if it was still there from a previous time
            os.makedirs(d)  # Make the directory. Otherwise we run into trouble.
        else:
            os.makedirs(d)  # Make the directory. Otherwise we run into trouble.
    return

def remove_file(f):
    """
    Ensure that file f does not exist.
    """
    if os.path.exists(f):
        os.remove(f)  # Just to be sure
    else:
        pass
    return

def getCSV(f):
    """
    Collect a given CSV file and put it in a Pandas DataFrame
    """
    df = pd.read_csv(f)
    return df

def import_arff(fname, encode=True):
    """
    Import an .arff datafile.
    """
    with open(fname, 'r') as f:
        data = arff.load(f, encode_nominal=encode)
    return data['attributes'], pd.DataFrame(data['data'])

# Generating actual names

def gen_fname(base, function, ext='', fold=None, target=None, sep='_'):
    """
    Helper function to make sure all the actual filenames in our system follow similar conventions.
    """

    name = base + sep + function  # The basic convention.

    # Encodings of folds and target sets
    if fold != None:
        name = name + sep + get_code(fold, 'fold')
    if target != None:
        name = name + sep + get_code(target, 'target')

    name = name + ext
    return name

def get_code(number, kind):
    """
    Function to generate specific codes that appear in our filenames.
    """
    if kind == 'fold':
        code_string = 'F' + str(number).zfill(2)
    elif kind == 'target':
        code_string = 'T' + str(number).zfill(3)
    elif kind == 'exp':
        code_string = str(number).zfill(3)  # No prefix here!
    else:
        print("Did not recognize kind of code to make!")
    return code_string

def get_input_names(input_dir, criterion):
    """
    Identifies the inputfiles in the provided folder.

    This by looking at extension and the word Train or Test in the filename.
    If more precise conditions are necessary, they can be added as an extra condition in the list comprehension.

    OUT: Two 1D-arrays of filenames.
    """

    all_input_files = os.listdir(input_dir)

    # We identify the correct files by Train or Test and the criterion, usually the extension
    train_data = [os.path.join(input_dir, name) for name in all_input_files if criterion in name if 'Train' in name]
    test_data =  [os.path.join(input_dir, name) for name in all_input_files if criterion in name if 'Test' in name]
    train_data.sort()
    test_data.sort()

    assert len(train_data) == len(test_data)  # To verfiy correct folds

    return train_data, test_data

def set_output_names(output_dir, system, ext, nb_folds, nb_target_sets):
    """
    Make a 2D array of outputdata filenames.
    pred[f][s], where f refers to the folds and s refers to the target set that was predicted.

    """
    # Init of 2D array of prediction filenames. pred[f][s]
    pred = [([None] * nb_target_sets) for f in range(0, nb_folds)]

    for f in range(0, nb_folds):
        pred[f] = [os.path.join(output_dir, gen_fname(system, 'Pred', ext, f, s)) for s in range(0, nb_target_sets)]

    return pred

# Extracting metadata

def count_lines(filename):
    """
    This function counts the amount of tuples in an .arff file.
    mmap avoids memory issues.
    """
    with open(filename, 'r+') as f:
        buf = mmap.mmap(f.fileno(), 0)
        lines = 0
        readline = buf.readline
        while readline():
            lines += 1

        for num, line in enumerate(f):
            if line == "@DATA" + "\n":
                startLine = num + 1  # Don't forget this +1
                break

    nbTuples = lines - startLine
    return nbTuples, startLine

def count_atts(filename):
    """
    This function counts the amount of atts in an .arff file.
    NB.: This only works in a preprocessed .arff file by our preprocessing system!
    Otherwise this fails completely.
    """
    with open(filename, 'r+') as f:
        for num, line in enumerate(f):
            if line == "@DATA" + "\n":
                nb_atts = num - 1
                break
    return nb_atts

def count_values(filename):
    """
    Give the number of possible values of the attributes in the .arff file.

    :param filename: Filename of the .arff file.
    :return: An array, the ith entry corresponds to the amount of attribute i
    """

    with open(filename, 'r') as f_in:
        dataset = arff.load(f_in)
        attributes = dataset['attributes']
        # att is a (name,values tuple)
        vals = [len(att[1]) for att in attributes]
    return vals

def get_classlabels(filename):
    """
    Fetch all the possible classlabels.

    :param filename: .arff file that we scan the header of.
    :return: Array of arrays. The ith entry contains an array [classlabel0, classlabel1, etc]
    """

    values = count_values(filename)
    classlabels = [list(range(i)) if type(i) == int else None for i in values]
    return classlabels

def count_atts_csv(filename):
    """
    Naive function that counts the amount of attributes in a csv file.
    """
    #TODO: Make this more efficient, loads the entire file.
    df = getCSV(filename)
    nb_atts = len(df.columns)
    return nb_atts

# Manifest stuff. Both used in analysis and running the exps.

def read_manifest(ID, inputsfile=None, data_loc=None):
    """
    Get the manifest (.json)

    Extract relevant information from the experiment manifest data.
    This manifest serves as an extra reference, but mainly for query information
    """

    # This is pretty long but in fact it is just walking through the standardised filesystem of the experiment.
    ID=int(ID)
    exp_code = get_code(ID, 'exp')
    main_folder = os.path.dirname(os.path.dirname(os.getcwd())) # Main folder of this experiment.

    exp_folder = os.path.join(main_folder, 'out', exp_code)
    assert len(os.listdir(exp_folder)) == 1                     # There can be no confusion about which experiment here.

    dataset_name = os.listdir(exp_folder)[0]
    dataset_folder = os.path.join(exp_folder, dataset_name)
    logs_folder = os.path.join(dataset_folder, 'logs')

    manifest_names = [os.path.join(logs_folder, name) for name in os.listdir(logs_folder) if 'manifest' in name]
    assert len(manifest_names) == 1  # We can allow just one manifest.

    with open(manifest_names[0]) as f:
        manifest = json.load(f)

    manifest['dataset'] = dataset_name  # We add the name of the dataset about which we fetch all the settings.
    return manifest

def flatten_manifest_to_settings_dict(manifest):
    """
    For convenience, I sometimes need a flat manifest.

    I leave the queries alone here, hence all the loops and checks.
    """
    d = {}
    for k, v in manifest.items():
        if type(v) == str:
            d[k] = v
        if type(v) == dict:
            for k1, v1 in v.items():
                if type(v1) != list:
                    d[k1] = v1
    return d

def get_queries(mf):
    """
    Extract the queries.

    :param mf: this is the manifest
    """

    d = mf['inference']['descsets']
    t = mf['inference']['targetsets']
    m = mf['inference']['missingsets']
    return d, t, m