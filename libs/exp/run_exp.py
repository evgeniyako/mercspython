"""
run_exp -  This actually controls the actual execution of experiments with the correct parameters.


These script are concerned with running the actual experiments
"""

import json
from abc import ABCMeta, abstractmethod

import numpy as np
import random as rnd
from scipy import special

from .libs.monitor import Logfile, JsonFile
from .libs.process import Process
from .tools.shared import *


# 1. Query Compilation

def compile_queries(atts, keyword='basic', query_param=1):
    """
    Compile attribute sets for induction and inference (or 'queries')

    By query we mean a certain inference task that is required from the model. Such an inference task
    is definded by an input set of attributes and an output set of attributes, both subsets of the total set of attributes of the dataset.

    :param atts - This is an array that contains all the attributes of the dataset
    :param keyword - Defines the query generation method
    :param query_param - Some methods of query compilation can take an additional parameter.
    """

    induction_desc, induction_targ = atts, atts
    nb_atts=len(atts)

    if keyword == 'basic':  # Leave-one-out prediction

        # query_targ_sets = [[i] for i in atts if i < 50]
        query_targ_sets = [[i] for i in atts]
        query_desc_sets = [list(set(atts) - set(qts)) for qts in query_targ_sets]  # All the remaing atts are used as inputs
        query_miss_sets = [[] for i in query_desc_sets]

    elif keyword == 'double':

        query_targ_sets = [[i - 1, i] for i in atts if i > 0 if i < 100]
        query_desc_sets = [list(set(atts) - set(qts)) for qts in query_targ_sets]  # All the remaining atts are used as inputs
        query_miss_sets = [[] for i in query_desc_sets]

    elif keyword == 'it_targetsets':
        """
        We build targetsets of different sizes.
        We specify the maximum amount of different sizes, and we specify an upper limit. This limit is a percentage of the total.
        Per size we specify the maximum amount of queries.
        This can be a difficult function because of the comb function, this might be left out.
        """


        # In total we want to ask no more then: max_nb_ts_sizes*max_nb_queries
        max_nb_ts_sizes = 20  # Amount of different targetset sizes we want to check
        max_nb_queries = 20  # Amount of queries per targetset size
        ratio_t_to_tot = 0.4  # Percentage of nb_atts that we want to predict in one query
        max_ts_size = nb_atts * ratio_t_to_tot  # Largest targetset that we allow

        targetset_sizes = np.unique(np.linspace(1, max_ts_size, num=max_nb_ts_sizes, dtype=int)).tolist()  # linspace + unique takes care of never taking too much different sizes

        query_targ_sets, query_desc_sets = [], []
        for ts_size in targetset_sizes:
            temp_query_targ_sets = []
            combs = int(special.comb(nb_atts, ts_size))
            nb_queries = np.min([max_nb_queries,
                                 combs])  # If the maximum number of queries cannot be computed, we settle for all the possible ones.

            while len(temp_query_targ_sets) < nb_queries:
                targetset = set(np.random.choice(atts, replace=False, size=ts_size).tolist())
                if targetset not in temp_query_targ_sets: temp_query_targ_sets.append(targetset)

            temp_query_targ_sets = [list(i) for i in temp_query_targ_sets]
            temp_query_desc_sets = [list(set(atts) - set(qts)) for qts in temp_query_targ_sets]  # No missing values

            query_targ_sets.extend(temp_query_targ_sets)
            query_desc_sets.extend(temp_query_desc_sets)

        query_miss_sets = [[] for i in query_desc_sets]

    elif keyword == 'it_missing':
        """
        We build missingsets of different sizes.
        Per size we ask a fixed amount of queries
        """

        # In total we want to ask no more than: max_nb_ms_size*max_nb_queries
        max_nb_queries = 5  # Amount of missingsets we generate PER size. Each different missingset results in a different query, hence the name.

        max_nb_ms_sizes = 20  # Amount of different missingset sizes we want to check
        ratio_m_to_tot = 0.4
        max_ms_size = nb_atts * ratio_m_to_tot  # Largest missingset that we allow (in absolute terms)

        missingset_sizes = np.unique(np.linspace(1, max_ms_size, num=max_nb_ms_sizes, dtype=int)).tolist()  # linspace + unique takes care of never taking too much different sizes

        query_targ_set = np.random.choice(atts, replace=False, size=1).tolist()

        query_miss_sets, query_desc_sets = [], []
        for ms_size in missingset_sizes:
            available_atts = list(set(atts) - set(query_targ_set))
            temp_query_miss_sets = []
            combs = int(special.comb(nb_atts, ms_size))
            nb_queries = np.min([max_nb_queries,
                                 combs])  # If the maximum number of queries cannot be computed, we settle for all the possible ones.

            while len(temp_query_miss_sets) < nb_queries:
                missingset = set(np.random.choice(available_atts, replace=False, size=ms_size).tolist())
                if missingset not in temp_query_miss_sets: temp_query_miss_sets.append(missingset)

            temp_query_miss_sets = [list(i) for i in temp_query_miss_sets]
            temp_query_desc_sets = [list(set(available_atts) - set(qms)) for qms in
                                    temp_query_miss_sets]  # No missing values

            query_miss_sets.extend(temp_query_miss_sets)
            query_desc_sets.extend(temp_query_desc_sets)

        query_targ_sets = [query_targ_set for i in query_desc_sets]

    elif keyword=='missing_ratio':
        """
        We build missingsets of a fixed size, given by query_param
        """

        max_nb_queries = 50                         # Amount of missingsets we generate
        ratio_m_to_tot = query_param
        ms_size = int(round(nb_atts * ratio_m_to_tot))   # Size of the missingset, based on the ratio provided as param

        query_targ_set = np.random.choice(atts, replace=False, size=1).tolist()

        query_miss_sets, query_desc_sets = [], []

        available_atts = list(set(atts) - set(query_targ_set))
        combs = int(special.comb(nb_atts, ms_size))
        nb_queries = np.min([max_nb_queries, combs])  # If the maximum number of queries cannot be computed, we settle for all the possible ones.

        while len(query_miss_sets) < nb_queries:
            missingset = set(np.random.choice(available_atts, replace=False, size=ms_size).tolist())
            if missingset not in query_miss_sets: query_miss_sets.append(missingset)

        query_miss_sets = [list(i) for i in query_miss_sets]
        query_desc_sets = [list(set(available_atts) - set(qms)) for qms in query_miss_sets]  # No missing values

        query_targ_sets = [query_targ_set for i in query_desc_sets]

    elif keyword=='it_missing_2':
        """
        We randomly select some attributes which will be targets.

        Each target is predicted in one query, all the rest is descriptive. Then we gradually leave out descriptive attributes.
        """

        query_desc_sets, query_targ_sets, query_miss_sets = [], [], []
        nb_diff_configs = 25 # This is an arbitrary parameter!

        np.random.seed(997)
        shuffled_atts = list(np.random.permutation(atts).tolist() for i in range(nb_diff_configs))

        orig_code = generate_query_code(nb_atts, miss_size=0)
        ms_steps = generate_ms_steps(orig_code, query_param) # Query param determines the amount of steps.
        code = orig_code.copy()

        for atts_shuffle in shuffled_atts:

            # Getting the query, and saving it
            desc, targ, miss = code_to_query(code, atts_shuffle)  # Generate the actual query (3 arrays)
            query_desc_sets.append(desc), query_targ_sets.append(targ), query_miss_sets.append(miss)

            for step in ms_steps:
                code = desc_to_miss(code, step)  # Update code

                # Getting the query, and saving it
                desc, targ, miss = code_to_query(code, atts_shuffle)
                query_desc_sets.append(desc), query_targ_sets.append(targ), query_miss_sets.append(miss)

            code = orig_code.copy()  # Do not forget to reset the code to its original form.

    elif keyword=='copy':
        """
        In this case, we copy the queries from another experiment.

        Experiment is specified by its ID, and the queries are extracted from the manifest.
        """
        mf=read_manifest(query_param)
        # TODO: Check if at least these queries are for the same dataset.

        induction_desc = mf['induction']['descriptive']
        induction_targ = mf['induction']['target']
        query_desc_sets = mf['inference']['descsets']
        query_targ_sets = mf['inference']['targetsets']

        try:
            query_miss_sets=mf['inference']['missingsets']
        except:
            print('No missing sets in the manifest')

    else:
        # TODO: This is yet to be conceived.
        print('INVALID KEYWORD')

    assert len(query_targ_sets) == len(query_desc_sets)  # This always has to be the case

    return induction_desc, induction_targ, query_desc_sets, query_targ_sets, query_miss_sets

## Helper functions

def generate_query_code(nb_atts, desc_size=None, targ_size=1, miss_size=None):
    """
    Generate an array of the same length as the original attribute array.

    0  means attribute is descriptive
    1  means attribute is target
    -1 means attribute is missing
    """

    # Some automatic procedures if we are given some freedom.
    if desc_size == None and miss_size == None:
        desc_size = nb_atts - targ_size  # All the rest is assumed descriptive
        miss_size = 0
    elif desc_size != None and miss_size == None:
        miss_size = nb_atts - targ_size - desc_size
    elif desc_size == None and miss_size != None:
        desc_size = nb_atts - targ_size - miss_size
    else:
        pass

    # Bad user choices can still cause meaningless queries, so we check
    assert ((desc_size + targ_size + miss_size) == nb_atts)
    assert (targ_size > 0)
    assert (desc_size > 0)

    # The actual encoding
    code = [0 for i in range(desc_size)]
    code.extend([+1 for i in range(targ_size)])
    code.extend([-1 for i in range(miss_size)])
    return code

def code_to_query(code, atts):
    """
    Change the code-array to an actual query, which are three arrays.

    :param code: Array that contains: 0-desc/1-target/-1-missing code for each attribute
    :param atts: Array that contains the attributes (indices)
    :return: Three arrays. One for desc atts indices, one for targets, one for missing
    """

    assert len(code) == len(atts)

    desc = [x for i, x in enumerate(atts) if code[i] == 0]
    targ = [x for i, x in enumerate(atts) if code[i] == 1]
    miss = [x for i, x in enumerate(atts) if code[i] ==-1]
    return desc, targ, miss

def desc_to_miss(code, amount=1):
    """
    Change the first 'amount' of 0's to -1's
    """

    changes, i = 0, 0

    while (i < len(code) and changes < amount):
        if code[i] == 0 :
            code[i] = -1
            changes += 1
        else:
            pass
        i+=1
    return code

def generate_ms_steps(code, param):
    """
    Generate an array that contains the (integer) steps in which we increase the set of missing attributes.

    This is in the context of converting descriptive attributes to missing ones.

    :param param -  If parameter is a float < 1, it is interpreted as the percentage increase that is desired
                    If parameter is a int > 1, it is interpreted as the amount of attributes that needs to be added
                    to the missing set at each step.
    :param code - The starting code
    """

    max_amount_steps = int(1 / param) if param < 1 else int(param)
    available_atts = code.count(0) - 1  # We have to keep at least one descriptive attribute, hence -1!

    ms_sizes = np.linspace(0, available_atts, num=max_amount_steps + 1, dtype=int).tolist()
    ms_steps = [ms_sizes[i] - ms_sizes[i - 1] for i in range(1, len(ms_sizes)) if (ms_sizes[i] - ms_sizes[i - 1] > 0)]

    return ms_steps

def get_ind_atts(atts):
    """

    :param atts:
    :return:
    """

    induction_desc, induction_targ = atts, atts
    return induction_desc, induction_targ

def get_atts(inputfile):
    """
    Get a list of attribute indices of ALL the attributes in the inputfile.

    :param inputfile: .arff or .csv file.
    :return: e.g. [0,1,2,3] for four-attribute dataset.
    """

    # TODO: Can this be done more cheaply?
    if '.arff' in inputfile:
        nb_atts = len(count_values(inputfile))  # The length of values is the amount of atts
    elif '.csv' in inputfile:
        nb_atts = count_atts_csv(inputfile)
    else:
        print("Inputsfile has invalid extension, could not count nb of attributes.")
        pass

    atts = [i for i in range(0, nb_atts)]
    return atts


# 2. Running the actual system

def run_exps(settings, datasets, data_loc=None):
    """
    This function runs an experiment.

    Such an experiment is characterized by its settings and identified by its ID-number.

    Info is a dictionary that contains MAINLY filesystem info.
    But because the filesystem implicitly contains information about the ID and system used anyway, we also put those in
    explicitly.
    """

    # TODO: This is still prepared for different datasets for an identical experiment.
    # This is not how it works anymore. (So all the distributed naming functions can be merged.)

    system = settings['system']

    info = generate_global_names(data_loc)                      # Global naming convention
    info = generate_exp_names(info, settings['ID'], system)     # Current exp (ID-defined) conventions

    ML_algo = get_ML_system(system)

    for dataset in datasets:
        """
        1] Make sure we have all the relevant filenames at our disposition in a dict.
            check_names makes sure all the directories are in place.
            And deletes them if they were still there! An eID uniquely identifies an experiment and thus overwrites
            previous iterations of the same experiment!
        """
        info = generate_dataset_names(info, dataset)  # Current dataset (name-defined) conventions
        delete_old_exps(info)  # Make sure that everything is empty before we start.

        """
        2] Generate the settings file for this specific experiment
        """
        ML_algo.make_settings(info, settings)

        """
        3] Run the experiment on the current dataset, with correct settings.
        """
        ML_algo.run_system(info)

        print('Dataset ' + dataset + ' is done.')

    return


# 3. Introducing classes

def get_ML_system(system):
    """
    Function that determines the correct class of ML system based on input parameter.

    :param system: A keyword given in the settings file that tells us which system should be used.
    :return: result: An object of the relevant class, contains the necessary methods.
    """
    #  TODO: Fix this in a more pythonic way.

    if system == 'mercs':
        result = minimercs()
    elif ((system == 'smile') | (system == 'bayesfusion')) :
        result = bayesfusion()
    elif system=='mercspython':
        result = mercspython()
    else:
        print('Did not recognize the requested ML system.')
    return result

class ML_system(object):
    """
    Represents a given ML algorithm that we want to use to perform experiments.

    One method is the same for all subclasses, although it does depend on the specific system.
    """

    __metaclass__ = ABCMeta

    def make_train_test_pred(self,info,settings):
        """
        Get all the necessary files for training, testing and predictions.

        :param info: Dict that contains information about the filesystem
        :param settings: Dict that contains the setting read from the .csv file with exp. params
        :param query_key: Keyword that specifies the kind of query to be used.
        :return: train, test, pred: Lists of filenames.
        """

        """
        1] Collect some useful information
        """
        system = settings['system']
        query_key=settings['queries']['keyword']
        query_param = settings['queries']['param']

        input_dir = info['xval']        # Input data dir
        output_dir = info['output']     # Output data dir
        input_data_ext = info[system]['ext']['in']
        output_data_ext = info[system]['ext']['out']

        """
        2] Inputnames, attribute sets (ind+inf) and outputnames.
        """
        # Inputnames (I fetch these based on the system name! I could also do this on extension)
        train, test = get_input_names(input_dir, system+'_')

        # Attribute sets for both induction and inference (or 'querying')
        atts=get_atts(train[0])
        ind_desc, ind_targ, q_desc_sets, q_targ_sets, q_miss_sets = compile_queries(atts,query_key,query_param)

        settings['induction']['descriptive'] = ind_desc
        settings['induction']['target'] = ind_targ
        settings['inference']['descsets'] = q_desc_sets
        settings['inference']['targetsets'] = q_targ_sets
        settings['inference']['missingsets'] = q_miss_sets

        # Outputnames
        nb_folds, nb_queries = len(train), len(q_targ_sets)  # nb_folds derived from inputfiles, queries from nb_query_tsets
        pred = set_output_names(output_dir, system, output_data_ext, nb_folds, nb_queries)

        return train, test, pred

    def write_manifest(self, info, settings):
        """
        Writing the settings dict in the logs folder.

        Contains lots of useful information.
        """

        file = os.path.join(info['logs'], 'manifest_E' + info['ID_code'] + '.json')
        with open(file, 'w') as f:
            json.dump(settings, f)
        return

    @abstractmethod
    def make_settings(self,info,settings):
        pass

    @abstractmethod
    def run_system(self, info):
        """
        Method that runs the actual system with the correct settings.

        :param info: Dict that contains the filesystem for our experiments
        :return: void
        """
        pass

class minimercs(ML_system):
    """
    Class that contains our minimercs java implementation.

    It is build around Clus, and is fully implemented by us.
    """

    def make_settings(self,info,settings):
        """
        Make the file that contains the Mercs settings.

        In the tradition of the Clus system, this file is human-readable,
        and even human-writeable if necessary.
        The price for this is that the actual function here is complicated.

        Cf. parent class for all info.
        """

        # 0. Extract train, test and prediction from the superclass make_settings method
        train, test, pred = ML_system.make_train_test_pred(self,info,settings)

        # 1. Init the settings data structure, with 'default' values

        s = {} # The dict that we will eventually print in the Mercs settings file.

        s['General'] = {'Verbose': 0}
        s['Attributes'] = {'Descriptive': None, 'Target': None}
        s['Data'] = {'File': None, 'TestSet': None, 'PruneSet': 0.00}

        """
        Options for Heuristic:
        
            Default, ReducedError, Gain, GainRatio, VarianceReduction, MEstimate, 
            Morishita, DispersionAdt, DispersionMlt, RDispersionAdt, RDispersionMlt
        """
        s['Tree'] = {'Heuristic': 'GainRatio'}
        s['Mercs'] = {'MercsBaseType': 'Ensemble', 'MercsPredictionMethod': 'Simple',
                      'MercsSelectionMethod': 'Base', 'MaxSelectionIterations': 1, 'MercsModelStorage': None}
        s['MercsTest'] = {'TestSet': None, 'Descriptive': None, 'Target': None, 'Outputfiles': None}

        """
        Options for EnsembleMethod:
            
            Boosting, Bagging, RForest, RSubspaces, BagSubspaces, RFeatSelection
            
        VotingType:
            Majority, ProbabilityDistribution
        """
        s['Ensemble'] = {'Iterations': 10, 'EnsembleMethod': 'RForest', 'VotingType': 'Majority',
                         'Optimize': 'No'}

        # 2. Fill the settings with user-specified values.

        s['Mercs']['MercsBaseType'] = settings['induction']['basetype']
        s['Ensemble']['EnsembleMethod'] = settings['induction']['ensemble']
        s['Ensemble']['Iterations'] = settings['induction']['ensemble_its']
        s['Mercs']['MercsSelectionMethod'] = settings['induction']['selection']
        s['Mercs']['MaxSelectionIterations'] = settings['induction']['selection_its']
        s['Mercs']['ShareOfTargets'] = settings['induction']['selection_share']
        s['Mercs']['NumPartitions'] = settings['induction']['selection_part']


        s['Mercs']['MercsPredictionMethod'] = settings['inference']['prediction']

        # Write the ind and inf (the queries) atts. We need to add one, the Mercs system counts from 1.
        s['Attributes']['Target'] = [i + 1 for i in settings['induction']['target']]
        s['Attributes']['Descriptive'] = [i + 1 for i in settings['induction']['descriptive']]

        s['MercsTest']['Target'] = [[i + 1 for i in ts] for ts in settings['inference']['targetsets']]
        s['MercsTest']['Descriptive'] = [[i + 1 for i in ds] for ds in settings['inference']['descsets']]

        assert len(s['MercsTest']['Target']) == len(s['MercsTest']['Descriptive'])

        # Per fold, we fill in the queries for the MercsTests.
        nb_folds = len(train)  # Since we write one settings file per fold, we also fill it in in this way.
        for f in range(0, nb_folds):
            s['temp'] = {}  # This is necessary because otherwise tests of other folds survive.

            # Fixing files for models
            m_dir, m_ext = info['models'], info['mercs']['ext']['model']
            m_fname = gen_fname('mercs', 'Model', m_ext, fold=f)
            s['Mercs']['MercsModelStorage'] = os.path.join(m_dir, m_fname)

            s['Data']['File'] = train[f]
            s['Data']['TestSet'] = train[f]

            s['MercsTest']['TestSet'] = test[f]
            s['MercsTest']['Outputfiles'] = pred[f]

            assert len(s['MercsTest']['Outputfiles']) == len(s['MercsTest']['Target'])

            # Make one node for every test. Nodename contains the outputfname...
            nb_preds = len(pred[f])
            for i in range(0, nb_preds):
                ID = i
                t = s['MercsTest']
                desc, target, out = t['Descriptive'][i], t['Target'][i], t['Outputfiles'][i]

                nodeName = 'MercsTest, Test{}'.format(str(ID).zfill(2))
                s['temp'][nodeName] = {'Descriptive': desc, 'Target': target, 'Output': out, 'TestSet': test[f]}



                # 3] Print the settings with user-specified values.

            s_dir, s_ext = info['settings'], info['mercs']['ext']['settings']
            s_fname = gen_fname('mercs', 'Settings', s_ext, fold=f)
            s_file = os.path.join(s_dir, s_fname)

            # Actual writing of the Mercs settings file
            with open(s_file, 'w') as file:
                for k, v in s.items():
                    if k == 'MercsTest':  # This node we do not print
                        pass
                    elif k == 'temp':  # This node holds all the actual paragraphs that contain the tests
                        for k2, v2 in v.items():
                            file.write('[{}] \n'.format(k2))
                            self.printNode(v2, file)
                            file.write('\n')
                    elif k == 'Ensemble':  # Only print when we need ensemble models as component
                        if s['Mercs']['MercsBaseType'] == 'Ensemble':
                            file.write('[{}] \n'.format(k))  # Print out the 'node' name, e.g. [Attributes]
                            self.printNode(v, file)  # Print out the settings contained in the node, e.g. verbosity=0
                            file.write('\n')
                        else:
                            pass
                    else:  # All this can be printed in a straighforward fashion.
                        file.write('[{}] \n'.format(k))  # Print out the 'node' name, e.g. [Attributes]
                        self.printNode(v, file)  # Print out the settings contained in the node, e.g. verbosity=0
                        file.write('\n')

        # 3. Write the manifest of this experiment in the log folder.

        ML_system.write_manifest(self, info, settings)

        return

    def run_system(self, info):
        """
        Method that runs the actual system with the correct settings.

        Cf. parent class for all info.
        """

        """
        0] Preliminaries
        """
        exc = info['mercs']['bin']

        # Collect settingsfiles
        s_dir = info['settings']
        s_fnames = os.listdir(s_dir)
        s_files = [os.path.join(s_dir, name) for name in s_fnames if 'mercs' in name]
        s_files.sort()

        nb_folds = len(s_files)
        tot_times = [None] * nb_folds
        """
        1] Run the external algorithm for each fold.
        """
        for f in range(0, nb_folds):
            # Fixing the logs
            log_name = gen_fname('mercs', 'Log', fold=f)
            log_path = os.path.join(info['logs'], log_name)  # Maybe this will cause errors, but I think not.
            json_path = os.path.join(info['logs'], log_name + '.json')

            monitors = [
                Logfile(log_path),
                JsonFile(json_path),
            ]

            # Running the external file
            cmd = ['java', '-jar', exc, s_files[f]]  # The actual command.

            p = Process(cmd, monitors=monitors)
            p.run()
            tot_times[f] = p.process_time

        return tot_times

    def printNode(self, dic, f):
        """
        Helper function to print one entry in the dict.
        """

        for k, v in dic.items():
            f.write(str(k) + ' = ')
            # If the value is an array, we print the contents comma-seperated, e.g. [1,2] -> 1,2
            if isinstance(v, (list, tuple)):
                cleanstring = ','.join(str(e) for e in v)
                f.write(cleanstring + '\n')
            else:
                f.write(str(v) + '\n')
        return

class bayesfusion(ML_system):
    """
    Class for the BayesFusion SMILE implementation.

    We wrote a wrapper to use this commercial Bayesian Networks package.
    """

    def make_settings(self,info,settings):
        """
        Make the file that contains the SMILE settings.

        This communicates with the extended wrapper that I wrote for this system.
        """

        # 0] Extract train, test and prediction from the superclass make_settings method
        train, test, pred = ML_system.make_train_test_pred(self, info, settings)

        # 1] Construct the actual settings

        s = {}

        s['targSets'] = settings['inference']['targetsets']
        s['descSets'] = settings['inference']['descsets']
        s['missSets'] = settings['inference']['missingsets']

        nb_folds = len(train)  # Since we write one settings file per fold, we also fill it in in this way.

        for f in range(0, nb_folds):
            s['trainSet'], s['testSet'], s['smilePred'] = train[f], test[f], pred[f]

            # Fixing files for models
            m_dir, m_ext = info['models'], info['smile']['ext']['model']
            m_fname = gen_fname('smile', 'Model', m_ext, fold=f)
            s['networkfile'] = os.path.join(m_dir, m_fname)

            # Fixing files for settings
            s_dir, s_ext = info['settings'], info['smile']['ext']['settings']
            s_fname = gen_fname('smile', 'Settings', s_ext, fold=f)
            s_file = os.path.join(s_dir, s_fname)

            # Writing it to a settings file
            with open(s_file, 'w') as file:
                json.dump(s, file)

        # 2] Write the manifest of this experiment in the log folder.
        ML_system.write_manifest(self,info, settings)

        return

    def run_system(self, info):
        """
        Method that runs the actual system with the correct settings.

        Cf. parent class for all info.
        """

        """
        0] Preliminaries
        """
        exc = info['smile']['bin']
        smile_lib_folder = info['smile']['lib']

        # Collect settingsfiles
        s_dir = info['settings']
        s_fnames = os.listdir(s_dir)
        s_files = [os.path.join(s_dir, name) for name in s_fnames if 'smile' in name]
        s_files.sort()

        nb_folds = len(s_files)
        tot_times = [None] * nb_folds

        """
        1] Run the external algorithm for each fold.
        """
        for f in range(0, nb_folds):
            # Fixing the logs
            log_name = gen_fname('smile', 'Log', fold=f)
            log_path = os.path.join(info['logs'], log_name)  # Maybe this will cause errors, but I think not.
            json_path = os.path.join(info['logs'], log_name + '.json')

            monitors = [
                Logfile(log_path),
                JsonFile(json_path),
            ]

            # Running the external file
            cmd = ['java', '-Djava.library.path=' + smile_lib_folder, '-jar', exc, s_files[f]]

            p = Process(cmd, monitors=monitors)
            p.run()
            tot_times[f] = p.process_time

        return tot_times

class mercspython(ML_system):
    """
    Python/sklearn implementation of the MERCS concept.

    To counteract the difficulties that go with using ensembles in Clus,
    I opted for a try with sklearn instead.
    """

    def make_settings(self,info,settings):
        # 0. Extract train, test and prediction from the superclass make_settings method
        train, test, pred = ML_system.make_train_test_pred(self, info, settings)

        # 1. Construct the actual settings

        s = {}

        s['query_targ'] = settings['inference']['targetsets']
        s['query_desc'] = settings['inference']['descsets']
        s['query_miss'] = settings['inference']['missingsets']

        s['Ensemble'] = {'Iterations': 10, 'EnsembleMethod': 'RForest'}
        s['Mercs'] = {'MercsBaseType': 'Ensemble', 'MercsPredictionMethod': 'Simple',
                      'MercsSelectionMethod': 'Base', 'MaxSelectionIterations': 1, 'ShareOfTargets': 0.1,
                      'NumPartitions': 1, 'MercsModelStorage': None}

        s['Ensemble']['EnsembleMethod'] = settings['induction']['ensemble']
        s['Ensemble']['Iterations'] = settings['induction']['ensemble_its']

        s['Mercs']['MercsBaseType'] = settings['induction']['basetype']

        # s['Mercs']['MercsSelectionMethod'] = settings['induction']['selection']
        s['Mercs']['SelectionIterations'] = settings['induction']['selection_its']
        s['Mercs']['NumTargets'] = settings['induction']['selection_num_targets']
        s['Mercs']['Heuristic'] = settings['induction']['selection_h']
        s['Mercs']['Iterations'] = settings['induction']['selection_its']

        s['Mercs']['MercsPredictionMethod'] = settings['inference']['prediction']

        nb_folds = len(train)  # Since we write one settings file per fold, we also fill it in in this way.

        for f in range(0, nb_folds):
            s['train_data'], s['test_data'], s['Outputfiles'] = train[f], test[f], pred[f]

            # Fixing files for models
            m_dir, m_ext = info['models'], info['mercspython']['ext']['model']
            m_fname = gen_fname('mercspython', 'Model', m_ext, fold=f)

            # Fixing files for settings
            s_dir, s_ext = info['settings'], info['mercspython']['ext']['settings']
            s_fname = gen_fname('mercspython', 'Settings', s_ext, fold=f)
            s_file = os.path.join(s_dir, s_fname)

            # Writing it to a settings file
            with open(s_file, 'w') as file:
                json.dump(s, file)

        # 2. Write the manifest of this experiment in the log folder.
        ML_system.write_manifest(self, info, settings)
        return

    def run_system(self, info):
        """
        Method that runs the actual system with the correct settings.

        Cf. parent class for all info.
        """

        """
        0] Preliminaries
        """
        exc = info['mercspython']['bin']

        # Collect settingsfiles
        s_dir = info['settings']
        s_fnames = os.listdir(s_dir)
        s_files = [os.path.join(s_dir, name) for name in s_fnames if 'mercspython' in name]
        s_files.sort()

        nb_folds = len(s_files)
        tot_times = [None] * nb_folds
        """
        1] Run the external algorithm for each fold.
        """
        for f in range(0, nb_folds):
            # Fixing the logs
            log_name = gen_fname('mercspython', 'Log', fold=f)
            log_path = os.path.join(info['logs'], log_name)  # Maybe this will cause errors, but I think not.
            json_path = os.path.join(info['logs'], log_name + '.json')

            monitors = [
                Logfile(log_path),
                JsonFile(json_path),
            ]

            # Running the external file
            cmd = ['python', exc, s_files[f]]  # The actual command.

            p = Process(cmd, monitors=monitors)
            p.run()
            tot_times[f] = p.process_time

        return tot_times


