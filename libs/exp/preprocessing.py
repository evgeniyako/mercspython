"""
Preprocessing - Module that handles all the preprocessing of our data.


Contains functions useful for preprocessing inputdata for our algorithm.
"""

import numpy as np
import datetime
from .tools.shared import *

# Preliminaries

def generate_input_names(fs, dataset):
    """
    Generate all the filenames relevant to the input

    We generate here the filenames relevant for the INPUT of the ML algorithm.
    N.b.: 'fs' stands for 'filesystem'.
    """

    # Everything actual data (I) related
    fs['raw'] = os.path.join(fs['path']['raw'], dataset)
    fs['clean'] = os.path.join(fs['path']['clean'], dataset)
    fs['preproc'] = os.path.join(fs['path']['preproc'])
    fs['xval'] = os.path.join(fs['path']['xval'], dataset)
    fs['xval_log'] = os.path.join(fs['path']['data_log'],'xval_log', dataset) # pp_log stands for preproc-log. This is dataset-specific.

    # Verify existence of these folders first
    check_dir([fs['raw'], fs['clean'], fs['preproc'], fs['xval'], fs['xval_log']])

    return fs


def export_arff(O_fname, header, data, name='cleandata'):
    """
    Export an .arff datafile.

    INPUT:
        O_fname: Name of the outputfile
        header:  The attributes of this .arff file, in (n,values) format
        data:    DataFrame that contains the actual data
        name:    The name of the dataset, if none provided, we just go with 'cleandata'

    """

    encoder = arff.ArffEncoder()

    firstline = "@RELATION" + " " + '"' + name + '"\n'

    with open(O_fname, 'w') as f:
        f.write(firstline)

        for attr in header:
            line = encoder._encode_attribute(attr[0], attr[1])
            print(line, file=f)

        print('@DATA', file=f)
        data.to_csv(f, index=False, header=False)

    return

def create_new_header(attr):
    nb_atts = len(attr)
    header = [] * nb_atts
    for att in range(nb_atts):
        # If it's a nominal attribute
        if (isinstance(attr[att][1], list)):
            nb_values = len(attr[att][1])
            header.append(('Var' + str(att), [str(i) for i in range(nb_values)]))
        else:
            # Otherwise, it's a numerical attribute
            header.append(('Var' + str(att), 'numeric'))
    return header


def drop_na(df):
    """
    Drop the NA values in a dataframe.
    """
    df.dropna(inplace=True)
    return df


def drop_atts(atts, data, atts_to_drop):
    """
    Drop a list of given attributes from the attributes and data.
    """

    atts = pd.DataFrame(atts).T  # Temporary conversion to dataframe
    atts.drop(atts.columns[atts_to_drop], axis=1, inplace=True)
    atts = atts.T.values.tolist()  # Again to its original form

    data.drop(data.columns[atts_to_drop], axis=1, inplace=True)
    return atts, data

def normalize_data(data, header):
    normalized_data = data
    nb_atts = data.shape[1]
    for att in range(nb_atts):
        # If current attribute is numerical
        if (~isinstance(header[att][1], list)):
            normalized_data[att] = (normalized_data[var] - np.min(normalized_data[att])) / (
            np.max(normalized_data[att]) - np.min(normalized_data[att]))
    return normalized_data

def enforce_type(header, data, fixed_type):
    """
    Enforce int type on the columns corresponding to nominal attributes.
    This is useful because Mercs gives errors when expects and int and gets a float.
    """
    nb_atts = data.shape[1]
    for att in range(nb_atts):
        if (isinstance(header[att][1], list)):
            data[att] = data[att].astype(fixed_type)
    return data

# Cleaning the data

def global_cleaning(dataset):
    # 1] Fixing the filesystem
    info = generate_global_names()
    info = generate_input_names(info, dataset)

    I_file = os.path.join(info['raw'], dataset + '.arff')
    O_file_name = gen_fname(dataset, 'Clean', ext='.arff')
    O_file = os.path.join(info['clean'], O_file_name)

    # 2] Importing raw data and mandatory cleaning.
    header, data = import_arff(I_file)
    drop_na(data)

    # 3]  Normalize the numerical values
    normalized_data = normalize_data(data, header)

    # 4] Inforce int type for nominal values
    enforce_type(header, data, int)

    # 5] Clean the header
    header = create_new_header(header)

    # 3] Saving the dataset
    export_arff(O_file, header, data, name=dataset)
    return

def drop_vars(file, vars_to_drop):
    """
    Function to drop variables from a dataset.


    Takes an array of vars to drop as input.
    """

    header, data = import_arff(file)
    header, data = drop_atts(header, data, vars_to_drop)
    enforce_type(data, int)
    header = create_new_header(header)
    export_arff(file, header, data)
    return


# Crossvalidation


def create_mask(nb_folds, size, seed):
    """
    Function that creates the mask that tells us to which fold a tuple belongs.

    Later on, we use this to create the actual crossvalidation.
    """

    np.random.seed(seed)
    partId = np.arange(0, nb_folds) # Partition ID, from 0 to nbParts-1
    fullReps = size // nb_folds      # This yields the total amount of FULL tiles we can do of our sequence.
    leftovers = size % nb_folds      # These are the entries that have not yet received a partition (last ones)
    leftoverIds = np.random.randint(nb_folds, size=leftovers)

    mask = np.append(np.tile(partId, fullReps), leftoverIds)
    np.random.shuffle(mask)
    return mask


def make_crossvalidation(dataset, data_loc=None, folds=10, seed=997):
    """
    Function that makes a crossvalidation for a given dataset.

    We assume a compatible nomenclature.
    """

    # 1] Fixing the filesystem and filenames
    info = generate_global_names(data_loc)
    info = generate_input_names(info, dataset)

    I_fname = gen_fname(dataset, 'Clean', ext='.arff')
    I_file = os.path.join(info['clean'], I_fname)

    O_dir, log_dir = info['xval'], info['xval_log']
    make_fresh_dir([O_dir,log_dir])  # Deleting a possible previous crossvalidation and logfiles

    train_files = [os.path.join(O_dir, gen_fname('mercs', 'Train', ext='.arff', fold=f)) for f in range(folds)]
    test_files = [os.path.join(O_dir, gen_fname('mercs', 'Test', ext='.arff', fold=f)) for f in range(folds)]
    log_file=os.path.join(log_dir, gen_fname(dataset,'xval_log',ext='.txt'))

    # 2] Preliminaries
    print('The random seed used for this crossvalidation is: ' + str(seed))
    nb_tuples, startline = count_lines(I_file)  # startline is the line where the data actually starts.
    mask = create_mask(folds, nb_tuples, seed)

    for f in range(folds):
        # 1] File handling
        org = open(I_file, 'r')
        train_set = open(train_files[f], 'w')
        test_set = open(test_files[f], 'w')

        # 2A] Write the headers of the files
        for num, line in enumerate(org):
            if num < startline:
                test_set.write(line)
                train_set.write(line)
            else:
                break

        # 2B] Write the tuples of this fold (i.e.: the ones with True testId)
        for num, line in enumerate(org, startline):
            if mask[num - startline] == f:
                test_set.write(line)
            else:
                train_set.write(line)

        train_set.close()
        test_set.close()
        org.close()

    make_crossval_log(log_file, I_file, dataset, seed, folds)

    return


def make_crossval_log(log_file, input_file, dataset, seed, folds):
    """
    Make a log of the crossvalidation operation.

    This keeps track of some facts to enhance reproducibility.
    :return:
    """
    if os.path.exists(log_file): os.remove(log_file)  # Just to be sure
    with open(log_file, 'w') as f:
        f.write('Inputfile   ' + input_file + '\n')
        f.write('Dataset     ' + dataset + '\n')
        f.write('Randomseed  ' + str(seed) + '\n')
        f.write('Folds       ' + str(folds)+'\n'+'\n')
        f.write('Date        ' + str(datetime.date.today()) + '\n')

    return


# Format conversion


def arff_to_csv(In, Out):
    """
    Convert a given .arff file to a regular .csv file.

    Explicitly hardcoded, this function eliminates all the extra .arff info.
    """

    arffInput = open(In, 'r')
    csvOutput = open(Out, 'w')

    # First we fix the header

    # Just to get the right names of the variables, in the csv-file
    dataset = arff.load(arffInput)
    attrs = dataset['attributes']
    size = len(attrs)
    new_header = []
    i = 1
    for attr in attrs:
        csvOutput.write(attr[0])
        new_header.append(attr[0])
        if i == size:
            csvOutput.write('\n')
        else:
            csvOutput.write(',')
        i += 1

    # Secondly we fix the data

    arffInput = open(In, 'r')  # arff load closes the file, so we need to open it again

    # Scanning until the data tag
    startLine = 0
    for num, line in enumerate(arffInput):
        if line == "@DATA" + "\n":
            startLine = num + 1
            break
        else:
            startLine = 1

    # Write all the data to the csv file
    for num, line in enumerate(arffInput, startLine):
        csvOutput.write(line)

    arffInput.close()
    csvOutput.close()
    return


def convert_mercs_inputs_to_bayesfusion(dataset, data_loc=None):
    """
    Conversion of inputfiles

    Convert all the .arff files for the Mercs system to BayesFusion(/smile)-suited .csv files.
    """

    info = generate_global_names(data_loc)
    info = generate_input_names(info, dataset)
    info = generate_names_mercs(info)

    # 1] Fetching filenames of the Mercs system
    input_dir = info['xval']
    input_ext = info['mercs']['ext']['in']

    mercs_train, mercs_test = get_input_names(input_dir, input_ext)
    assert len(mercs_train) == len(mercs_test)

    # 2] Generating filenames for the SMILE system
    info = generate_names_smile(info)  # Add the relevant SMILE nomenclature.
    output_dir = info['xval']
    output_ext = info['smile']['ext']['in']

    nb_folds = len(mercs_train)
    smile_train = [os.path.join(output_dir, gen_fname('smile', 'Train', output_ext, f)) for f in range(nb_folds)]
    smile_test = [os.path.join(output_dir, gen_fname('smile', 'Test', output_ext, f)) for f in range(nb_folds)]

    # 3] Doing the actual conversion
    for f in range(nb_folds):
        arff_to_csv(mercs_train[f], smile_train[f])
        arff_to_csv(mercs_test[f], smile_test[f])

    return


# Copy to experiment-ready folder


def export_data_for_experiment(exp_name, datasets=None, folds=None, systems=None, data_loc=None):
    """
    Extract the desired files from the preprocessed bunch.

    In case of big data, preprocessing can be a long task. When it is finally done, maybe sometimes for quick
    experiments, we just want a single fold. Or, the raw data never has to go to the actual experiment. In these cases,
    this function can quickly extract a portion of the preprocessed datafiles.

    :param exp_name:  The name of the experiment. This is just for the folder name that this function will create.
    :param datasets:  Array of dataset names, e.g.: ['nursery','income']
    :param folds:     Array of ints, specifying the folds that we want to extract.
    :param systems:   Array of strings, specifying the inputfiles for which system we need, e.g.: ['mercs','smile']
    :param data_loc:  The data_location parameter, which is often used in this package. Cf. elsewhere for its meaning.
    :return:
    """

    # Preliminaries
    info = generate_global_names(data_loc)
    exp_data = os.path.join(info['path']['out'], 'data_' + exp_name)
    exp_clean = os.path.join(exp_data, 'clean')
    exp_prep = os.path.join(exp_data, 'preproc')
    exp_xval = os.path.join(exp_prep, 'crossval')

    make_fresh_dir([exp_data, exp_clean, exp_prep, exp_xval])

    clean_contents, xval_contents = os.listdir(info['path']['clean']), os.listdir(info['path']['xval'])

    if datasets == None: datasets = os.listdir(info['path']['xval'])  # In this case, we take all the preprocessed datasets
    if systems == None: systems = ['mercs']  # Easy solution

    for dataset in datasets:

        # Preliminaries
        assert (dataset in xval_contents and dataset in clean_contents)

        ds_clean_src = os.path.join(info['path']['clean'], dataset)
        ds_xval_src = os.path.join(info['path']['xval'], dataset)
        ds_clean_dest = os.path.join(exp_clean, dataset)
        ds_xval_dest = os.path.join(exp_xval, dataset)

        # The 'clean' folder
        shutil.copytree(ds_clean_src, ds_clean_dest)

        # The 'crossval' folder
        make_fresh_dir([ds_xval_dest])

        ds_xval_src_contents = []
        for s in systems:
            train, test = get_input_names(ds_xval_src, s)
            max_nb_folds = len(train)
            ds_xval_src_contents.extend(train)
            ds_xval_src_contents.extend(test)

        if folds == None: folds = list(range(max_nb_folds))  # Easy solution
        relevant_files = [f for f in ds_xval_src_contents for x in folds if get_code(x, 'fold') in f]

        for f in relevant_files:
            shutil.copy2(f, ds_xval_dest)

    # The 'data_log' folder
    log_file = os.path.join(info['path']['data_log'], gen_fname(exp_name, 'export_log', ext='.txt'))
    make_export_log(log_file, exp_name, datasets, folds, systems)
    shutil.copytree(info['path']['data_log'], os.path.join(exp_data,'data_log'))

    return

def make_export_log(log_file, exp_name ,datasets, folds, systems):
    """
    Make a log of the export operation.

    This keeps track of some facts to enhance reproducibility.
    :return:
    """
    if os.path.exists(log_file): os.remove(log_file)  # Just to be sure

    with open(log_file, 'w') as f:
        f.write('For experiment:       ' + exp_name + '\n')
        f.write('For systems:          ' + str(systems)+ '\n')
        f.write('Datasets exported:    ' + str(datasets)+ '\n')
        f.write('Folds selected        ' + str(folds)+ '\n' + '\n')

        f.write('Date        ' + str(datetime.date.today()) + '\n')

    return