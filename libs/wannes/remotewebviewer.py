#!/usr/bin/env python3
# encoding: utf-8
"""
experimenter.remoteviewer - Listen to remote experiments

__author__ = "Wannes Meert"
__copyright__ = "Copyright 2016 KU Leuven, DTAI Research Group"
__license__ = "APL"

..
    Part of the DTAI experimenter code.

    Copyright 2016 KU Leuven, DTAI Research Group

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import os
import sys
import logging
import socket
import queue
import json
import threading

from flask import Flask, Response

from experimenter.remote.pull import Pull, logger
from experimenter.utils import level_type


static_folder = os.path.join(os.path.dirname(os.path.abspath(__file__)), "web", "static")
app = Flask(__name__, static_folder=static_folder)


@app.route('/')
def mainfile():
    with open(os.path.join(static_folder, 'monitor.html')) as f:
        data = f.read()
    return Response(data, mimetype="text/html")


class Producer(threading.Thread):

    def __init__(self, queue, lock):
        threading.Thread.__init__(self)
        self.queue = queue
        self.lock = lock

    def run(self):
        description = (
            "RemoteViewer listens to experimenter instances that push messages. "
            "Add the experimenter.remote.Push ProcessMonitor to the list of monitors "
            "when starting a Process instance with as host the ip address of the instance "
            "where this script runs."
        )

        logger.setLevel(logging.ERROR - 10 * (0))
        logger.addHandler(logging.StreamHandler(sys.stdout))

        hosts = [
            # socket.gethostbyname(socket.gethostname()),
            socket.gethostname()
        ]
        for host in hosts:
            print("Current host: {}".format(host))

        receiver = PullLock(port=5556, lock=self.lock, queue=self.queue)
        receiver.run()

q = queue.Queue()
lock = threading.Condition()

producer = Producer(q, lock)
producer.start()


@app.route("/stream")
def stream():
    return Response(Consumer(q, lock), mimetype="text/event-stream")


class PullLock(Pull):

    def __init__(self, port, lock, queue):
        Pull.__init__(self, port)
        self.lock = lock
        self.queue = queue


    def process_msg(self, node, identifier, msg, ts) -> None:
        """Do something with the message send by a node.
        :param node: Name of node that sends the message
        :param identifier: Log level according to process.levels
        :param msg: The actual message
        :param ts: Timestamp in ISO format
        """

        global q
        global lock

        lock.acquire()
        q.put({
            "node": node,
            "type": level_type[identifier],
            "msg": msg,
            "ts": ts
        })
        lock.notify()
        lock.release()


class Consumer(object):

    def __init__(self, queue, lock):
        self.queue = queue
        self.lock = lock

    def __iter__(self):

        global q
        global lock

        while True:
            lock.acquire()
            if q.empty():
                lock.wait()
            elem = q.get()
            lock.release()
            yield 'data:' + json.dumps(elem) + '\n\n'


def start_flask():
    app.run()


if __name__ == "__main__":
    start_flask()

