import os
import sys
import py.test

from experimenter.process import Function
from experimenter.monitor import Print, Logfile, MemoryLimit, TimeLimit, FileSizeLimit, JsonFile

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__))))
import my_binary


def test_run_no_monitors():
    p = Function(target=my_binary.main, args=())
    result = p.run()

    assert result == 0


def test_run_all_monitors(delay=0):
    log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testlog")
    success_path = log_path + ".success.log"
    running_path = log_path + ".running.log"
    json_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testlog.json")

    try:
        os.remove(running_path)
        os.remove(success_path)
    except FileNotFoundError:
        pass

    monitors = [
        Print(), JsonFile(json_path),
        Logfile(log_path, force=True),
        JsonFile(json_path),
        MemoryLimit(maxmem=1000, minavailable=1),
        TimeLimit(1000),
        FileSizeLimit(filename=running_path, maxsize=1 * 1024 * 1024)
    ]

    p = Function(target=my_binary.main, args=(delay,), monitors=monitors)
    result = p.run()

    assert os.path.exists(success_path)
    assert result == 0


def test_run_logging():
    log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testlog")
    success_path = log_path + ".success.log"
    running_path = log_path + ".running.log"

    try:
        os.remove(running_path)
        os.remove(success_path)
    except FileNotFoundError:
        pass

    p = Function(target=my_binary.main, args=(), monitors=[Print(), Logfile(log_path, force=True)])
    result1 = p.run()

    p = Function(target=my_binary.main, args=(), monitors=[Print(), Logfile(log_path, force=False)])
    result2 = p.run()

    assert os.path.exists(success_path)
    assert result1 == 0
    assert result2 is None


if __name__ == "__main__":
    # test_run_no_monitors()
    test_run_all_monitors(delay=0.2)
    # test_run_logging()
