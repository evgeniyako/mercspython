import os
import py.test

from experimenter.process import Process
from experimenter.monitor import Print, Logfile, MemoryLimit, TimeLimit, FileSizeLimit, JsonFile


binary_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "my_binary.py")


def test_run_no_monitors():
    p = Process(["python3", binary_path])
    p.run()


def test_run_all_monitors():
    log_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testlog")
    success_path = log_path + ".success.log"
    running_path = log_path + ".running.log"
    json_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "testlog.json")

    try:
        os.remove(running_path)
        os.remove(success_path)
    except FileNotFoundError:
        pass

    monitors = [
        Print(),
        Logfile(log_path),
        JsonFile(json_path),
        MemoryLimit(maxmem=1, minavailable=1),
        TimeLimit(100),
        FileSizeLimit(filename=running_path, maxsize=1 * 1024 * 1024)
    ]

    cmd = ["python3", binary_path]

    p = Process(cmd, unbuffered=False, monitors=monitors, cwd=None)
    p.run()

    assert os.path.exists(success_path)

if __name__ == "__main__":
    test_run_no_monitors()
