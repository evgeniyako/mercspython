import sys
import random
import time

def main(delay=0):
    numbers = []
    nb_it = 100
    nb_el = 100000

    print("start")
    for i in range(nb_it):
        print("iteration {}".format(i))
        numbers.append([random.random() for j in range(nb_el)])
        time.sleep(delay)
    print("done")
    return 0

if __name__ == "__main__":
    sys.exit(main())
