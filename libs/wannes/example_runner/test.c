#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main()
{
    printf("Start (stdout)\n");
    fprintf(stderr, "Start (stderr)\n");
//    raise(SIGSEGV);  // Test a segfault
	double *array;
	int i=0;
	int j=0;
	int num=10000;
	for (i=0; i<250; i++) {
		printf("Iteration %d\n", i);
//		fflush(stdout);  // Test with and without flushing
		array = (double*)malloc(num*sizeof(double));
		if (array != NULL) {
		    for (j = 0; j < num; j++) {
		        array[j] = rand();
		    }
		} else {
		  printf("Allocating memory failed");
		}
		usleep(20000);
	}
}
