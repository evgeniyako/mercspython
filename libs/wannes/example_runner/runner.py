#!/usr/bin/env python3
# encoding: utf-8
"""
runner - Experimenter example binary runner

This example script allows you to run the experimenter code from the command
line.

__author__ = "Wannes Meert"
__copyright__ = "Copyright 2016 KU Leuven, DTAI Research Group"
__license__ = "APL"

..
    Part of the DTAI experimenter code.

    Copyright 2016 KU Leuven, DTAI Research Group

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import sys
import os
import argparse
import logging
import shlex

this_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.join(this_dir, '..'))
from experimenter.runner import run_external_binary, build_parser_general, build_parser_experimenter, usage_examples, \
    get_log_lvl
from experimenter.utils import logger


def run(args):
    logger.setLevel(get_log_lvl(**vars(args)))
    logger.addHandler(logging.StreamHandler(sys.stdout))

    if args.cmd is None:
        args.cmd = [this_dir+'/test']
    else:
        args.cmd = shlex.split(args.cmd)

    if not args.nobinary:
        run_external_binary(**vars(args))


def main(argv=None):
    parser = argparse.ArgumentParser(description='Run a binary through an experimenter process',
                                     epilog=usage_examples("./runner.py"),
                                     formatter_class=argparse.RawTextHelpFormatter)
    build_parser_general(parser)
    build_parser_experimenter(parser)
    parser.add_argument('--nobinary', '-b', action='store_true', help='Do not run external binary')
    args = parser.parse_args(argv)
    run(args)


if __name__ == "__main__":
    sys.exit(main())
