# Experimenter

A toolbox to run experiments using Python. Developed for the [DTAI research group](https://dtai.cs.kuleuven.be) at
[KU Leuven](http://www.kuleuven.be).

This package supports:
- Start a process using a subprocess or using a callable object 
- Print process output to (log/json)file and terminal simultaneously
- Simulate terminal for binaries with stdout buffering
- Print time and memory at intervals
- Kill at timeout
- Kill at memory limit or insufficient free memory
- Kill at file size limit
- Cache previous runs and avoid reruns
- Send monitoring information to remote viewer


## Usage

You can call the process you want to monitor as a subprocess (e.g. an external
binary) or as a callable object (e.g. a function). If you do not want to
integrate the Experimenter code in your own code you can also use the packaged
example 'runner.py' script. 

### Process

Usage follows that of `subprocess.Popen`.

    from experimenter.process import Process
    Process(["/path/to/binary", "-v"], monitors=[]).run()

Extra arguments starting with `popen_` are passed through to the `subprocess.Popen`.
The `run()` call returns the return code of the subprocess.

### Function

Usage follows that of `multiprocessing.Process`.

    from experimenter.process import Function
    import myscript
    Function(target=myscript.run, args=(1,"a"), monitors=[]).run()

The `run()` call returns the value returned by the target function.

### Runner CLI

An example runner command line application is available in the
`example_runner` directory.

    $ ./example_runner/runner.py -h

### Monitors

Print monitors:

- `experimenter.monitor.Print`
- `experimenter.monitor.Logfile`
- `experimenter.monitor.JsonFile`

Usage monitors:

- `experimenter.monitor.MemoryLimit`
- `experimenter.monitor.TimeLimit`
- `experimenter.monitor.FileSizeLimit`

Remote monitors:

- `experimenter.remote.push.PushToRemote`  
  Start `remoteviewer.py` or `remotewebviewer.py` on the machine that you will
  use to monitor the experiments.

For example usages see also `examples/example.py`, `tests/test_process.py` and
`tests/test_process.py`.

If you want to write your own monitor, inherit the class `experimenter.ProcessMonitor`.


## Dependencies

Made for Python 3.5 or higher.

Requires the following Python packages:

- [psutil](https://github.com/giampaolo/psutil) (not for `remoteviewer.py`)
- [pyzmq](https://github.com/zeromq/pyzmq) (if you use the `PushToRemote` process monitor)
    

## Platforms

Tested on:

- OS X 10.11-12


## Contact

- [Wannes Meert](https://people.cs.kuleuven.be/wannes.meert)  
  [Wannes.Meert@cs.kuleuven.be](mailto:Wannes.Meert@cs.kuleuven.be)
- [Anton Dries](https://people.cs.kuleuven.be/anton.dries)  
  [Anton.Dries@cs.kuleuven.be](mailto:Anton.Dries@cs.kuleuven.be)


## contributors

- Wannes Meert (DTAI - KU Leuven)
- Anton Dries (DTAI - KU Leuven)
- Jonas Vlasselaer (DTAI - KU Leuven)


## License

    DTAI experimenter code.

    Copyright 2016-2017 KU Leuven, DTAI Research Group

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
