#!/usr/bin/env python3
# encoding: utf-8
"""
experimenter.remoteviewer - Listen to remote experiments

__author__ = "Wannes Meert"
__copyright__ = "Copyright 2016 KU Leuven, DTAI Research Group"
__license__ = "APL"

..
    Part of the DTAI experimenter code.

    Copyright 2016 KU Leuven, DTAI Research Group

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
"""
import sys
import argparse
import logging
import socket
from experimenter.remote.pull import Pull, logger


def main(argv=None):
    description = (
        "RemoteViewer listens to experimenter instances that push messages. "
        "Add the experimenter.remote.Push ProcessMonitor to the list of monitors "
        "when starting a Process instance with as host the ip address of the instance "
        "where this script runs."
    )
    parser = argparse.ArgumentParser(description=description)
    parser.add_argument('--verbose', '-v', action='count', help='Verbose output')
    parser.add_argument('--port', '-P', default="5556", type=int, help='Port to connect to')
    args = parser.parse_args(argv)

    logger.setLevel(logging.ERROR-10*(0 if args.verbose is None else args.verbose))
    logger.addHandler(logging.StreamHandler(sys.stdout))

    hosts = [
        socket.gethostbyname(socket.gethostname()),
        socket.gethostname()
    ]
    for host in hosts:
        print("Current host: {}".format(host))

    receiver = Pull(port=args.port)
    receiver.run()
    # Override Pull.process_msg if you want to do something else than
    # simply printing the message


if __name__ == "__main__":
    sys.exit(main())

