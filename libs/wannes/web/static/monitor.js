
var settings = {
    nb_points: 1000,
    shift: true,
    fields: [
        {key: "mem(MiB)",      label: "Memory (MiB)"},
        {key: "%mem",          label: "%Memory"},
        {key: "%cpu",          label: "%CPU"},
        {key: "filesize(MiB)", label: "File (MiB)"},
    ],
    backgroundColorTrans: 0.0,
}


var charts = {"a": 1};
var backgroundColors = [
    'rgba(255, 99,  132, '+settings.backgroundColorTrans+')',
    'rgba(54,  162, 235, '+settings.backgroundColorTrans+')',
    'rgba(255, 206, 86,  '+settings.backgroundColorTrans+')',
    'rgba(75,  192, 192, '+settings.backgroundColorTrans+')',
    'rgba(153, 102, 255, '+settings.backgroundColorTrans+')',
    'rgba(255, 159, 64,  '+settings.backgroundColorTrans+')'
];
var borderColors = [
    'rgba(255, 99,  132, 1)',
    'rgba(54,  162, 235, 1)',
    'rgba(255, 206, 86,  1)',
    'rgba(75,  192, 192, 1)',
    'rgba(153, 102, 255, 1)',
    'rgba(255, 159, 64,  1)'
];

// Connect points though null values
Chart.pluginService.register({
    beforeDatasetsDraw: function(chart) {
        for (dataset of chart.config.data.datasets) {
            for(data of dataset._meta[chart.id].data) {
                data._model.skip = false;
                data._model.tension = 0;
            }
        }
    }
});

function updateChart(data) {
    var node = data.node;
    if (charts[node] === undefined) {
        createChart(node);
    }

//    var cpu = data.msg["%cpu"];
//    var mem = data.msg["mem(MiB)"];
//    var utime = data.msg["usertime"];
//    var stime = data.msg["systime"];
//    var atime = stime + utime;
//    var dtime = utime - charts[node].time;
//    charts[node].time = utime;

    var chart = charts[node];
    chart.size++;
    for (var idx=0; idx<settings.fields.length; idx++) {
        var value = data.msg[settings.fields[idx].key];
        // if this value is not included with this update, it will push 'undefined'
        chart.datasets[settings.fields[idx].key].data.push(value);
        if (chart.size >= settings.nb_points && settings.shift) {
            chart.datasets[settings.fields[idx].key].data.shift();
        }
    }

    chart.labels.push(moment(data.ts));
    if (chart.size >= settings.nb_points && settings.shift) {
//        chart.labels.push(chart.size);
        chart.labels.shift();
    }
    if (chart.lastupdate === null || moment().diff(chart.lastupdate, 's') >= 1) {
        chart.lastupdate = moment()
        chart.chart.update();
    }

    if (typeof data.msg === 'string') {
        data.msg = {'msg': data.msg};
    }

    var table = $('<table/>');
    var tr;
    for (var property in data.msg) {
        tr = $('<tr/>');
        tr.append("<td>" + property + "</td>");
        tr.append("<td>" + data.msg[property] + "</td>");
        table.append(tr);
    }
    var table_div = document.getElementById("table-"+node);
    $(table_div).empty()
    $(table_div).append(table);
}

function createChart(node) {
    var chartsContainer = $("#charts");
    var div = $("<div id='"+node+"'  class='node-container' />");
    chartsContainer.append(div);
    var title = $("<h3>"+node+"</h3>");
    div.append(title);
    var chart_div = $("<div id='chart-"+node+"'/>");
    div.append(chart_div);
    var chart_ctx = $("<canvas id='chart-ctx-"+node+"' widht='500' height='300'/>");
    chart_div.append(chart_ctx);
    var table_div = $("<div id='table-"+node+"'/>");
    div.append(table_div);

    var datasets = {};
    for (var idx=0; idx<settings.fields.length; idx++) {
        var data = [];
//        if (settings.shift) {
//            new Array(settings.nb_points).fill(0);
//        }
        datasets[settings.fields[idx].key] = {
            data: data,
            label: settings.fields[idx].label,
            backgroundColor: backgroundColors[idx],
            borderColor: borderColors[idx],
            borderWidth: 1,
        };
    }

    var labels = [];
//    if (settings.shift) {
//        for (var i=0; i<settings.nb_points; i++) {
//    //        labels.push(0); // (i);
//            labels.push(moment().subtract(settings.nb_points-i, 's'))
//        }
//    }
    var labelfmt = "MMM D kk:mm:ss";

    var myChart = new Chart(chart_ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: settings.fields.map(function(field) {
                var dataset = datasets[field.key];
                return {
                    data: dataset.data,
                    label: dataset.label,
                    backgroundColor: dataset.backgroundColor,
                    borderColor: dataset.borderColor,
                    borderWidth: dataset.borderWidth,
                };
            }),
        },
        options: {
            responsive: false,
            animation: false,
            scales: {
                xAxes: [{
                    type: 'time',
                    time: {
                      displayFormats: {
                        'millisecond': labelfmt,
                        'second': labelfmt,
                        'minute': labelfmt,
                        'hour': labelfmt,
                        'day': labelfmt,
                        'week': labelfmt,
                        'month': labelfmt,
                        'quarter': labelfmt,
                        'year': labelfmt,
                      }
                    }
                }],
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            }
        }
    });

    var size = 0;
//    if (settings.shift) {
//        size = settings.nb_points
//    }
    charts[node] = {
        chart: myChart,
        labels: labels,
        datasets: datasets,
        label: 1,
        size: size,
        lastupdate: null,
//        time: 0,
    }
}


$(document).ready(function() {
    var targetContainer = document.getElementById("stream");
    var eventSource = new EventSource("/stream");

    eventSource.addEventListener('message', function(e) {
        var data = JSON.parse(e.data);
        //targetContainer.innerHTML = e.data;
        updateChart(data);
    }, false);

}); // end doc.ready
