# Python Installation

## Local Install

In case the centrally installed version of Python is not up-to-date or you need to install custom libraries.


### Locally install packages

    $ pip3 install psutil --user


### Install package locally from source

    $ wget https://pypi.python.org/packages/78/cc/f267a1371f229bf16db6a4e604428c3b032b823b83155bd33cef45e49a53/psutil-4.3.1.tar.gz
    $ tar -xvzf psutil-4.3.1.tar.gz
    $ cd psutil-4.3.1
    $ python3.5 setup.py --user


### Install locally in a virtualenv

    $ pip3 install virtualenv
    $ virtualenv venv
    $ . venv/bin/activate
    $ pip3 install psutil


### Install Python and package from source

*This is not recommended, please ask the sysadmins to update to a recent version of Python 3 and the `python-dev` package.*

You can replace `$HOME` with the directory you prefer. For example if you want to avoid that Python counts towards your quota you can install to the temporary scratch space on the DTAI Pinacs by using the prefix path `/export/home1/NoCsBack/dtai/yourusername`.

First install Python:

    $ wget https://www.python.org/ftp/python/3.5/Python-3.5.tgz
    $ tar -xvzf Python-3.5.tgz
    $ cd Python-3.5
    $ ./configure --prefix=$HOME
    $ make install
    $ # Ideally the next lines should be in .bashrc
    $ export PATH=$HOME/bin:$PATH
    $ export LD_LIBRARY_PATH=$HOME/lib:$LD_LIBRARY_PATH

You can now use `pip3` to install packages as above. If you prefer a manual install from source:

    $ wget https://pypi.python.org/packages/78/cc/f267a1371f229bf16db6a4e604428c3b032b823b83155bd33cef45e49a53/psutil-4.3.1.tar.gz
    $ tar -xvzf psutil-4.3.1.tar.gz
    $ cd psutil-4.3.1
    $ python3 setup.py install --prefix=$HOME


### Install Python Anacondo

Instead of a installation from source, you can also use the [Anaconda distribution](https://www.continuum.io/downloads).

