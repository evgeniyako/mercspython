# MercsPython

## Intro

This is a python, sklearn based implementation of the Mercs idea

## Example usage

This is structured just like the previous experiments done with Mercs, and builds on the same 'exp' package that has been expanded to run the mercspython system.

To run an example experiment, switch to src/core and run:

`python parallel.py run_a_py_script params/00_test.csv 1`

This should work outside the box.

## Source files

Currently, the entire mercspython system is one .py script, located in the bin folder. The reason for this location is because in the end we want to create an executable that is compatible with our experimental code that also run BayesFusion and Mercs-Clus. 

Eventually, we might want to divide the system across different files and create a package from them. 
