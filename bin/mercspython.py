from sklearn.tree import *
from sklearn.ensemble import *
from sklearn.cluster.bicluster import SpectralBiclustering
from sklearn.cluster import AgglomerativeClustering
from sklearn.cluster import KMeans
import numpy as np
import pandas as pd
from sklearn.preprocessing import Imputer
from sklearn.feature_selection import *
from timeit import default_timer
import json
import sys
from random import *

"""
Important: This is the source code of mercspython.py

To have it work with all the scripts that run on pinacs etc, it is placed
in the 'bin' folder of the experiment, just like the jars of minimercs and smile.
"""

def selection_algo(atts, is_numerical, p = 1, W = None, nb_iterations = 1):
    """
    Selection algorithm.

    Each tree in ensemble predicts share*(total_num_attributes) targets.
    If p = 0, each tree predicts 5 attributes.

    Each target is predicted once within every partition of the attributes.

    """
    nb_atts = len(atts)
    # nb_numerical_atts = sum(is_numerical)
    # nb_nominal_atts = sum(is_nominal)

    # Compute number of targets per tree

    if (p == 0):
        nb_out_atts = 5
    elif (p >= 1):
        nb_out_atts = p
    else:
        nb_out_atts = int(np.round(p*nb_atts))

    # Compute number of models per iteration
    # Numerical and nominal attributes are predicted by different trees
    nb_models_it = int(np.ceil(nb_atts/nb_out_atts))
    nb_models = int(nb_iterations*nb_models_it)

    #print (nb_atts, nb_out_atts, nb_models_part, nb_models)
    codes = [[]] * nb_models  # one code per model

    for tree in range(nb_models):
        codes[tree] = [0] * nb_atts  # We start with everything descriptive

    for partition in range(nb_iterations):
        for attribute in range (len(atts)):
            probabilities = None
            if (W is None):
                # Random grouping of targets
                random_model = np.random.choice(np.arange(nb_models_it))
                while(np.sum(codes[partition*nb_models_it+random_model]) >= nb_out_atts):
                    random_model = np.mod((random_model+1),nb_models_it)
                model = random_model
            else:
                # Compute the heuristic function (= relatedness of the attribute to partial models)
                # Relatedness to a partial model -is equal to
                #   - mean relatedness to other targets in the model, if it's not empty
                #   - mean relatedness to all the attrubutes, if th model is empty
                mean_relatedness = W.mean()[attribute]
                h = np.matmul(codes, W[[attribute]].values)
                h[h == 0] = mean_relatedness
                # probabilities = np.ravel(h/sum(h))
                most_related_list = np.argsort(h, axis = 0)[::-1]
                index = 0
                model = most_related_list[index]
                # Find the  best model that can still have targets
                while((np.sum(codes[partition * nb_models_it + model]) >= nb_out_atts)):
                    index = np.mod(index+1,nb_models)
                    model = most_related_list[index]
            codes[partition * nb_models_it + model][attribute] = 1
    codes = np.array(codes)

    return codes



## Actual induction
def induce_mde_model(train_data_df, mdemodel_codes, model_settings):
    """
    Actual induction of the mde model.

    Returns a list of sklearn classifiers.
    """

    # data.iloc[:,is_numerical] - columns with numeric attributes only
    is_numerical = (train_data_df.dtypes=='float').values


    desc, targ, _ = codes_to_query(mdemodel_codes)
    nb_models = len(targ)
    train_data = train_data_df.values

    if model_settings['Mercs']['MercsBaseType'] != 'Ensemble':
        models = [DecisionTreeRegressor() for i in range(nb_models)]
    elif model_settings['Ensemble']['EnsembleMethod'] == 'Rforest':
        nb_trees = int(model_settings['Ensemble']['Iterations'])
        models = [RandomForestRegressor(n_estimators=nb_trees,
                                        max_features = 0.33) for i in range(nb_models)]
    else:
        print('Did not recognize this type of model. Using regular decision trees instead.')
        models = [DecisionTreeClassifier() for i in range(nb_models)]

    for i in range(nb_models):
        targ_atts = targ[i]
        desc_atts = desc[i]

        X = train_data[:, desc_atts]
        Y = train_data[:, targ_atts]

        models[i].fit(X, Y)
    return models


## Query-handling
def codes_to_query(codes, atts=None):
    if atts == None: atts = list(range(len(codes[0])))
    nb_models = len(codes)

    desc_sets = []
    targ_sets = []
    miss_sets = []

    for i in range(nb_models):
        desc, targ, miss = code_to_query(codes[i], atts)

        desc_sets.append(desc)
        targ_sets.append(targ)
        miss_sets.append(miss)

    return desc_sets, targ_sets, miss_sets

def code_to_query(code, atts=None):
    """
    Change the code-array to an actual query, which are three arrays.

    :param code: Array that contains: 0-desc/1-target/-1-missing code for each attribute
    :param atts: Array that contains the attributes (indices)
    :return: Three arrays. One for desc atts indices, one for targets, one for missing
    """

    if atts == None: atts = list(range(len(code)))
    assert len(code) == len(atts)

    desc = [x for i, x in enumerate(atts) if code[i] == 0]
    targ = [x for i, x in enumerate(atts) if code[i] == 1]
    miss = [x for i, x in enumerate(atts) if code[i] == -1]
    return desc, targ, miss

def query_to_code(desc_sets, targ_sets, miss_sets):
    """
    Summarize all the queries in one array.
    """
    assert len(desc_sets) == len(targ_sets)
    assert len(targ_sets) == len(miss_sets)

    nb_queries = len(desc_sets)
    nb_atts = len(desc_sets[0]) + len(targ_sets[0]) + len(miss_sets[0])
    codes = [[]] * nb_queries

    for i in range(nb_queries):
        codes[i] = [0 for i in range(nb_atts)]

        targ = targ_sets[i]
        miss = miss_sets[i]
        for t in targ:
            codes[i][t] = 1
        for m in miss:
            codes[i][m] = -1

    return codes


# Inference

def predict(test_data_df, mdemodel, mdemodel_codes, query_code, imputator, inf_settings, task = 'regression'):
    # print("Running new version!")

    assert len(mdemodel) == len(mdemodel_codes)
    assert len(query_code) == len(mdemodel_codes[0])

    nb_models = len(mdemodel)
    m_desc, m_targ, _ = codes_to_query(mdemodel_codes)
    q_desc, q_targ, _ = code_to_query(query_code)

    # Building a prediction strategy
    if inf_settings['Mercs']['MercsPredictionMethod'] == 'Simple':
        thr_steps = [-1]
    elif inf_settings['Mercs']['MercsPredictionMethod'] == 'Loose':
        thr_steps = np.arange(1.0, -0.2, -0.1)
    else:
        print('Did not recognize prediction method: ' + inf_settings['Mercs']['MercsPredictionMethod'] +
              '\n . Using simple algorithm instead')
        thr_steps = [0]

    for threshold in thr_steps:
        pred_strat = [can_model_activate(mdemodel_codes[i], query_code, threshold) for i in range(nb_models)]
        act_models = [i for i, v in enumerate(pred_strat) if v == 1]
        # print(act_models)
        if len(act_models) > 0: break

    X = create_test_data_query(test_data_df, query_code, imputator)

    result = [0] * len(q_targ)

    for m in act_models:
        X_new = X[:, m_desc[m]]
        Y = mdemodel[m].predict(X_new)
        Y = pd.DataFrame(Y).values  # To always get a 2D array as result.
        if (task == 'calssification'):
            Y = Y.astype(int)
        result = update_result(result, Y, m_targ[m], query_code, inf_settings['nb_vals'])

    if (task =='classification'):
        # Do majority vote here.
        for i in range(len(result)): result[i] = np.argmax(result[i], axis=1)
    else:
        # Predict the mean value
        for i in range(len(result)): result[i] = result[i]/len(act_models)

    return result

def update_result(R, Y, m_targ, q_code, nb_vals, task = 'regression'):
    """
    Extracts the columns from Y, the prediction of the model that are shared with the codes indicated
    as target in q_code.

    :param R:
    :param Y:
    :param m_targ:
    :param q_code:
    :param nb_vals:
    :return:
    """

    _, q_targ, _ = code_to_query(q_code)

    for Y_ind, att in enumerate(m_targ):
        if q_code[att] == 1:
            R_ind = q_targ.index(att)
            if (task == 'classification'):
                nb_classes = nb_vals[att]
                # Y[:,Y_ind] is relevant column of the prediction.
                one_hot = np.eye(nb_classes, dtype=int)[Y[:, Y_ind]]  # One-hot encoding.
                R[R_ind] += one_hot

            if (task == 'regression'):
                R[R_ind] += Y[:,Y_ind]
    return R

def can_model_activate(m_code, q_code, threshold=-0.1):
    """
    Determine whether or not a model can activate.

    """
    assert len(m_code) == len(q_code)

    m_desc, m_targ, _ = code_to_query(m_code)
    q_desc, q_targ, _ = code_to_query(q_code)

    t_cnt = len(set(m_targ) & set(q_targ))

    d_cnt = len(set(m_desc) & set(q_desc))
    d_ratio = d_cnt / len(m_desc)

    result = 1 if ((t_cnt > 0) & (d_ratio > threshold)) else 0

    return result

def create_test_data_query(test_data_df, query_code, imputator):
    """
    This function creates the test data for a given query.

    This means that it sets the unknown attributes first to NaN and then imputes them.

    :param test_data_df:
    :param query_code:
    :param imputator:
    :return:
    """

    query_data_df = test_data_df.copy()

    for i, v in enumerate(query_code):
        if v == -1:
            query_data_df.iloc[:, i] = np.nan

    query_data = imputator.transform(query_data_df)
    return query_data.astype(int)

def write_predictions_to_file(results, filename, query_code):
    """
    Given a filename and a numpy array of predictions, we write our predictions
    in the correct format.
    """

    targ_query = ['Var' + str(i) for i, v in enumerate(query_code) if v == 1]  # Building the header of the .csv file.

    df = pd.DataFrame(results).T

    df.to_csv(filename, header=targ_query, index=False)

    return


# Full run

def run_one_fold(settingsfile):
    # 0. Preliminaries
    ## 0.1 Settings
    with open(settingsfile) as json_file:
        settings = json.load(json_file)

    ## 0.2 Data-wrangling
    train_data_df, test_data_df = pd.read_csv(settings['train_data']), pd.read_csv(settings['test_data'])
    nb_vals = train_data_df.max().values + 1
    atts = list(range(len(nb_vals)))
    # data.iloc[:,is_numerical] - columns with numeric attributes only
    is_numerical = (train_data_df.dtypes == 'float').values

    # 1. Induction

    ## 1.1 Selection algorithm

    sel_settings = {'NumTargets': settings['Mercs']['NumTargets'],
                    'Heuristic': settings['Mercs']['Heuristic'],
                    'Iterations': settings['Mercs']['SelectionIterations']}

    nb_samples = int(np.round(0.2 * train_data_df.shape[0]))
    # Base Selection Strategy:
    # nb_iterations (possibly multi-target) Random Forest per group of p attributes
    # attributes are split in groups of p randomly.

    if(sel_settings['Heuristic'] == 'base'):

        mdemodel_codes = selection_algo(atts=atts,
                                        is_numerical=is_numerical,
                                        p=sel_settings['NumTargets'],
                                        W=None,
                                        nb_iterations=sel_settings['Iterations'])

    # Heuristic Selection Strategy
    # nb_iterations (possibly multi-target) Rendom Forests per group of p attributes
    # correlated attributes are more likely to be grouped together

    if (sel_settings['Heuristic'] == 'correlation'):

        C = train_data_df.corr()
        C = np.abs(C)
        C.values[[np.arange(len(C.columns))] * 2] = 0
        mdemodel_codes = selection_algo(atts=atts,
                                        is_numerical=is_numerical,
                                        p=sel_settings['NumTargets'],
                                        W=C,
                                        nb_iterations=sel_settings['Iterations'])

    if (sel_settings['Heuristic'] == 'inv_correlation'):

        C = train_data_df.corr()
        C = np.abs(C)
        C = 1/C
        C.values[[np.arange(len(C.columns))] * 2] = 0
        mdemodel_codes = selection_algo(atts=atts,
                                        is_numerical=is_numerical,
                                        p=sel_settings['NumTargets'],
                                        W=C,
                                        nb_iterations=sel_settings['Iterations'])

    # Heuristic Selection Strategy
    # nb_iterations (possibly multi-target) Rendom Forests per group of p attributes
    # attributes with high MI score are more likely to be grouped together

    if(sel_settings['Heuristic'] == 'mutual_info'):

        MI = get_mutual_info(train_data_df)
        mdemodel_codes = selection_algo(atts=atts,
                                        is_numerical=is_numerical,
                                        p=sel_settings['NumTargets'],
                                        W=MI,
                                        nb_iterations=sel_settings['Iterations'])

    # Heuristic Selection Strategy
    # nb_iterations (possibly multi-target) Rendom Forests per group of p attributes
    # attributes are more likely to be grouped together if they have similar set of important features
    # clustering done by biclustering algo

    if(sel_settings['Heuristic'] == "splits"):
        S = get_splits(train_data_df)
        n_clusters = int(sel_settings['NumTargets'])
        model = KMeans(n_clusters=n_clusters, random_state=997)
        model.fit(S)
        att_clustering = model.labels_
        print(att_clustering)
        mdemodel_codes = labels_to_codes(att_clustering)

    if(sel_settings['Heuristic'] == "feature_importance_similarity"):
        F = get_feature_importance(train_data_df, random_forest=False)
        nb_atts = train_data_df.shape[1]
        D = np.zeros((nb_atts,nb_atts),float)
        for i in range(nb_atts):
            for j in range(nb_atts):
                if (i != j):
                    ind = list(range(nb_atts))
                    ind.remove(i)
                    ind.remove(j)
                    D[i][j] = sum((F[i][ind]-F[j][ind])**2)
        D = pd.DataFrame(D)
        D = (D.T / D.T.max()).T
        mdemodel_codes = selection_algo(atts=atts,
                                        is_numerical=is_numerical,
                                        p=sel_settings['NumTargets'],
                                        W=D,
                                        nb_iterations=sel_settings['Iterations'])

    if (sel_settings['Heuristic'] == "feature_importance_spectral"):
        F = get_feature_importance(train_data_df[:nb_samples], random_forest=True)
        nb_atts = train_data_df.shape[1]
        #n_clusters = (int(sel_settings['NumTargets']), int(nb_atts/3))
        n_clusters = (int(sel_settings['NumTargets']), 2)
        model = SpectralBiclustering(n_clusters=n_clusters, method='log', random_state=997)
        model.fit(F)
        att_clustering = model.row_labels_
        mdemodel_codes = labels_to_codes(att_clustering)

    # Heuristic Selection Strategy
    # nb_iterations (possibly multi-target) Rendom Forests per group of p attributes
    # attributes are more likely to be grouped together if they have similar set of important features
    # clustering done with kmeans

    if (sel_settings['Heuristic'] == "feature_importance_kmeans"):
        F = get_feature_importance(train_data_df.iloc[:nb_samples],random_forest=True)
        n_clusters = int(sel_settings['NumTargets'])
        model = KMeans(n_clusters=n_clusters, random_state=997)
        model.fit(F)
        att_clustering = model.labels_
        mdemodel_codes = labels_to_codes(att_clustering)

    if (sel_settings['Heuristic'] == "feature_importance_single"):
        F = get_feature_importance(train_data_df.iloc[:nb_samples],random_forest=False)
        nb_atts = train_data_df.shape[1]
        #n_clusters = (int(sel_settings['NumTargets']), int(np.sqrt(nb_atts)))
        n_clusters = (int(sel_settings['NumTargets']), int(nb_atts/3))
        model = SpectralBiclustering(n_clusters=n_clusters, method='log', random_state=997)
        model.fit(F)
        att_clustering = model.row_labels_
        mdemodel_codes = labels_to_codes(att_clustering)

    if (sel_settings['Heuristic'] == "feature_importance_agglomerative"):
        F = get_feature_importance(train_data_df.iloc[:nb_samples],random_forest=False)
        n_clusters = int(sel_settings['NumTargets'])
        model = AgglomerativeClustering(n_clusters=n_clusters)
        model.fit(F)
        att_clustering = model.labels_
        mdemodel_codes = labels_to_codes(att_clustering)

    if (sel_settings['Heuristic'] == "pairwise"):
        # Building a RF for each pair of attributes
        nb_atts = train_data_df.shape[1]
        mdemodel_codes = get_pairwise_models(nb_atts)

    if (sel_settings['Heuristic'] == "triplets"):
        # Building a RF for each pair of attributes
        nb_atts = train_data_df.shape[1]
        mdemodel_codes = get_triplets_models(nb_atts)

    if (sel_settings['Heuristic'] == "single"):
        # Building a RF for each pair of attributes
        nb_atts = train_data_df.shape[1]
        mdemodel_codes = get_single_models(nb_atts)

    print(mdemodel_codes)

    ## 1.2 Actual induction
    ind_settings = {'Mercs': settings['Mercs'], 'Ensemble': settings['Ensemble']}

    start_ind = default_timer()
    if (sel_settings['Heuristic'] == 'base'):
        mdemodel = induce_mde_model(train_data_df, mdemodel_codes, ind_settings)
    else:
        mdemodel = induce_mde_model(train_data_df.iloc[nb_samples:], mdemodel_codes, ind_settings)
    stop_ind = default_timer()
    print_log_time(start_ind, stop_ind, "induction")

    # 2. Inference
    desc_sets, targ_sets, miss_sets = settings['query_desc'], settings['query_targ'], settings['query_miss']
    query_codes = query_to_code(desc_sets, targ_sets, miss_sets)

    inf_settings = {'Mercs': settings['Mercs'],'nb_vals':nb_vals}

    imputator = Imputer(missing_values='NaN', strategy='most_frequent', axis=0)
    imputator.fit(train_data_df)

    start_inf = default_timer()
    for i in range(len(query_codes)):
        ## 2.1 Actual inference
        res = predict(test_data_df, mdemodel, mdemodel_codes, query_codes[i], imputator, inf_settings)

        ## 2.2 Writing outputs
        outputfilename = settings['Outputfiles'][i]
        write_predictions_to_file(res, outputfilename, query_codes[i])
    stop_inf = default_timer()
    print_log_time(start_inf, stop_inf, "inference")
    print_nb_queries(len(query_codes))

    return


def get_single_models(nb_atts):
    nb_models = int(nb_atts * (nb_atts - 1))
    codes = [[]] * nb_models
    m = 0
    for i in range(nb_atts):
        for j in range(nb_atts):
            if (i != j):
                model = [-1] * nb_atts
                model[i] = 1
                model[j] = 0
                codes[m] = model
                m = m + 1
    return codes

def get_pairwise_models(nb_atts):
    nb_models = int(nb_atts*(nb_atts-1)/2)
    # We start with everything descriptive
    codes = [[]] * nb_models
    m = 0
    for i in range(nb_atts):
        for j in range(i+1,nb_atts):
            model = [0]*nb_atts
            model[i]=1
            model[j]=1
            codes[m] = model
            m = m+1
    return codes

def get_triplets_models(nb_atts):
    nb_models = int(nb_atts*(nb_atts-1)*(nb_atts-2)/6)
    # We start with everything descriptive
    codes = [[]] * nb_models
    m = 0
    for i in range(nb_atts):
        for j in range(i+1,nb_atts):
            for k in range(j+1,nb_atts):
                model = [0]*nb_atts
                model[i]=1
                model[j]=1
                model[k]=1
                codes[m] = model
                m = m+1
    return codes

def get_mutual_info(data):
    nb_atts = data.shape[1]
    mi = [0] * nb_atts
    for att in range(nb_atts):
        targets = list(range(nb_atts))
        targets.remove(att)
        mi[att] = mutual_info_regression(data, data[[att]], random_state=2017)
    MI = pd.DataFrame(mi)
    ## Attributes cannot be grouped with themselves
    MI.values[[np.arange(len(MI.columns))] * 2] = 0
    ## Scale the MI score
    MI = (MI.T / MI.T.max()).T
    return MI

def get_splits(data):
    nb_atts = data.shape[1]
    S = np.zeros((nb_atts,nb_atts),float)
    for att in range(nb_atts):
        tree = DecisionTreeRegressor()
        targets = list(range(nb_atts))
        targets.remove(att)
        X = data[targets]
        y = data[[att]]
        tree.fit(X, y)
        f = list(tree.tree_.feature)
        f = list(np.unique(f))
        f.remove(-2)
        for i in range(len(f)):
            if (f[i]>=att):
                f[i]=f[i]+1

        for i in range(nb_atts):
            S[att][i] = int(i in f)
    return S

def get_feature_importance(data,random_forest=True):
    nb_atts = data.shape[1]
    fi = [0]*nb_atts

    if(random_forest==True):
        for att in range(nb_atts):
            rf = RandomForestRegressor(n_estimators=30, max_features='sqrt')
            targets = list(range(nb_atts))
            targets.remove(att)
            X = data[targets]
            y = data[[att]]
            rf.fit(X, y)
            fi[att] = rf.feature_importances_
    else:
        for att in range(nb_atts):
            tree = DecisionTreeRegressor()
            targets = list(range(nb_atts))
            targets.remove(att)
            X = data[targets]
            y = data[[att]]
            tree.fit(X, y)
            fi[att] = tree.feature_importances_
    #F = np.ones((nb_atts, nb_atts), float)
    F = np.zeros((nb_atts, nb_atts), float)
    for i in range(nb_atts):
        for j in range(nb_atts):
            if i > j:
                F[i][j] = fi[i][j]
            if i < j:
                F[i][j] = fi[i][j - 1]
    return F

def labels_to_codes(att_clustering):
    nb_models = np.max(att_clustering)+1
    nb_atts = att_clustering.size
    codes = [[]]*nb_models
    for tree in range(nb_models):
        codes[tree] = [0] * nb_atts  # We start with everything descriptive

    for tree in range(nb_models):
        for att in range(nb_atts):
            if (att_clustering[att] == tree):
                codes[tree][att] = 1
    return codes

def print_log_time(start,stop,function):
    result=int(round((stop - start) * 1000))
    message="Total "+function+" time of the MDEModel was: "+str(result)+" ms";
    print("====================================================")
    print(message)
    print("====================================================")
    return

def print_nb_queries(nb_queries):
    message="This was for: " +  str(nb_queries) + " queries.";
    print("====================================================")
    print(message)
    print("====================================================")
    return

# Executable

def main():
    settingsfile=sys.argv[1]
    run_one_fold(settingsfile)


"""
Comment this function if in testing mode. 
Testing mode= using the jupyter notebook to pilot new functionalities before adding them to this notebook.

Uncomment when using as executable script!
"""

if __name__ == '__main__': main()
