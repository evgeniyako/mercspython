# exp 00

## it_miss_BF
IDs:1-11
The bayesfusion prediction on all datasets

## it_miss_s
IDs: 12-22
Simple prediction in Mercs. I have edited a bit the behaviour.
Default models get selected when no descriptive attributes are available.

## loose_beta
IDs: 23
First test of the loose algo to see if there is any difference.

## loose_small_ds
IDs: 23-27
Loose algo on small datasets

## loose_large_ds
IDs: 28-33
Loose algo on the other datasets

## it_small_ds
IDs: 34-38
First test of the iterative algorithm, now free from bugs


