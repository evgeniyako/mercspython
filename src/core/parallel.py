"""
This is the first in a series of three scripts that cooperate.
1) parallel.py 
This script cuts the inputs.csv file and extracts the header and the input values, and gives them as environment variables to a bash script.

It runs this bash script multiple times, on multiple cores.

2) run_a_py_script
This does almost nothing, just change the filename of the script you want to run,
and fix the environment etc (source activate)

3) The actual experiment, which is referenced in the run_a_py_script


To run this: 

$python parallel.py run_a_py_script params/00_test.csv 1

The last number is the number of cores.
"""

import sys, os,subprocess
from multiprocessing import Pool

batch = sys.argv[1]
data = open(sys.argv[2])
nbCores = int(sys.argv[3])

header = data.readline().rstrip().split(',')

lines = map(lambda line: line.rstrip().split(','), data.readlines())


def executeLine(line):
  env=os.environ.copy()
  for k,v in zip(header,line):
    print(k,v)
    env[k]=v
  subprocess.call('./'+batch, env=env)


p = Pool(nbCores)
p.map(executeLine, lines)






