
# coding: utf-8

# # Importing folder that contains our package

import sys
import os

src_folder = os.path.dirname(os.getcwd())       # Current file is in a subfolder 'ipynb' or 'core' of the src directory
main_folder = os.path.dirname(src_folder)       # Main folder of the experiment (= where everything runs)

py_pkg_folder = os.path.join(main_folder,'libs')
if py_pkg_folder not in sys.path: sys.path.append(py_pkg_folder)
    
import exp.run_exp as run
import exp.preprocessing as pp


# # Import settings and run
def main():
    ID=sys.argv[1]
    system=sys.argv[2]
    dataset=sys.argv[3]
    basetype=sys.argv[4]
    ensemble=sys.argv[5]
    ens_its=int(sys.argv[6])
    sel_num_targets=float(sys.argv[7])
    sel_heuristic=sys.argv[8]
    sel_its=int(sys.argv[9])

    prediction=sys.argv[10]
    query_keyword=sys.argv[11]
    query_param=float(sys.argv[12])

    settings={'ID':ID,'system':system}
    settings['inference']={}
    settings['induction']={}
    settings['induction']['basetype']=basetype
    settings['induction']['ensemble']=ensemble
    settings['induction']['ensemble_its']=ens_its

    settings['induction']['selection_num_targets']=sel_num_targets
    settings['induction']['selection_h']=sel_heuristic
    settings['induction']['selection_its']=sel_its


    settings['inference']['prediction']=prediction
    
    settings['queries']={}
    settings['queries']['keyword']=query_keyword
    settings['queries']['param']=query_param

    print('ID of current experiment: ',ID)
    
    run.run_exps(settings,[dataset])

if __name__ == '__main__':
    main()
